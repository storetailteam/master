import Slider from "./slider/index.js"
import Buttons from "./buttons/index.js"

class Mobile {
    constructor(payload) {
        this.payload = payload;
    }

    formatMobile(format, breakpoint) {
        let that = this;
        //set viewport
        this.setViewport(breakpoint);
        window.addEventListener('resize', () => that.setViewport(breakpoint));

        switch (format) {
            case "slider":
                let slider = new Slider(this.payload);
                slider.init();
                break;
            default:

        }
        switch (format) {
            case "buttons":
                let slider = new Buttons(this.payload);
                slider.init();
                break;
            default:
        }
    }

    setViewport(breakpoint) {
        const container = this.payload.html;
        if (window.matchMedia('(max-width: ' + breakpoint + 'px)').matches) {
            container.setAttribute("data-viewport", "mobile");
        } else {
            container.setAttribute("data-viewport", "desktop");
        }
    }


}

export default Mobile
