import sliderStyle from './main.less';

class Slider {
    constructor(payload) {
        this.html = payload.html;
        this.tracker = payload.tracker;
    }

    init() {
        sliderStyle.use();
        this.sliderMobileFormat();
        this.slider();
    }

    sliderMobileFormat() {
        //set attribute data-mobile
        this.html.setAttribute('data-mobile', 'slider');

        //inject dummy div (usefull for slider padding)
        let dummyDivSlider = document.createElement('div');
        dummyDivSlider.className = "sto-dummy-slider";
        this.html.querySelector(".sto-product-container").append(dummyDivSlider);
    }

    slider() {
        // create indicators
        let that = this;
        let sliderIndicatorsDiv = document.createElement('ul');
        sliderIndicatorsDiv.className = "sto-slider-indicators";
        this.html.querySelector(".sto-format").append(sliderIndicatorsDiv);
        const products = document.querySelectorAll('.sto-product-container>.sto-item');
        for (let i of products) {
            const id = i.getAttribute('data-id');
            sliderIndicatorsDiv.innerHTML += "<li data-id='" + id + "'></li>";
        }
        sliderIndicatorsDiv.querySelectorAll("li")[0].className += "current";


        // slider
        const sliderIndicators = sliderIndicatorsDiv.querySelectorAll('li');
        const sliderContainer = this.html.querySelector('.sto-product-container');
        let sliderContainerWidth = sliderContainer.offsetWidth / 2 - 5;
        let itemWidth = products[0].offsetWidth + 10;
        const productsNb = products.length;
        let timer = null;
        let currentProdIndex;

        // resize
        window.addEventListener('resize', function() {
            sliderContainer.scrollTo(scroll, 0);
            itemWidth = products[0].offsetWidth + 10;
            sliderContainerWidth = sliderContainer.offsetWidth / 2 - 5;

            for (let k of sliderIndicators) {
                k.className = "";
            }
            sliderIndicators[0].className += "current";
        });

        sliderContainer.addEventListener("scroll", function() {
            if (timer !== null) {
                clearTimeout(timer);
            }
            let scrollLeft = sliderContainer.scrollLeft + sliderContainerWidth;
            timer = setTimeout(function() {
                currentProdIndex = Math.floor(scrollLeft / itemWidth);
                sliderContainer.scrollTo({
                    left: (itemWidth * currentProdIndex) - (sliderContainer.offsetWidth / 10 - 10),
                    behavior: 'smooth',
                });
                for (let i of sliderIndicators) {
                    i.className = "";
                }
                sliderIndicators[currentProdIndex].className = "current";

            }, 100);
        });



        // setTimeout(() => {
        //     if (!isTrackFired) {
        //         let id = products[currentProdIndex].getAttribute("data-productid");
        //         that.tracker.browse({
        //             "tl": id,
        //             "pi": id
        //             // "pl": j.querySelector(".search-service-productDetails .search-service-productTitle>.LinesEllipsis").innerText
        //         });
        //         isTrackFired = true;
        //     }
        // }, 1000);



    }
}

export default Slider;
