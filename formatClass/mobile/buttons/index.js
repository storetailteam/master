import buttonsStyle from './main.less';

class Buttons {
    constructor(payload) {
        this.html = payload.html;
        this.tracker = payload.tracker;
        this.settings = payload.settings;
    }

    init() {
        buttonsStyle.use();
        this.buttonsMobileFormat();
        this.buttonsStyle()
        this.buttonsSetup(this.html);
    }

    buttonsMobileFormat() {
        //set attribute data-mobile
        this.html.setAttribute('data-mobile', 'buttons');
    }

    buttonsSetup(container, crawl) {
        if (!container.querySelector('.sto-buttons-wrapper')) {
            let that = this,
                products = that.settings.products,
                buttonWrapper;
            const stoItem = container.querySelectorAll('.sto-product-container>.sto-item');

            buttonWrapper = document.createElement('div');
            buttonWrapper.className = "sto-buttons-wrapper";
            container.querySelector('.sto-image-container').append(buttonWrapper);


            // set data index sto items
            for (let [index, u] of stoItem.entries()) {
                u.setAttribute(`data-index`, index);
            }

            for (let [index, p] of products.entries()) {
                const button = document.createElement('div');
                button.className = 'sto-button sto-button-normal';
                button.setAttribute('data-index', index);
                button.setAttribute('data-id', p);
                button.style.width = this.settings.button_width && this.settings.button_width != "" ? this.settings.button_width + 'px' : "150px";
                button.style.fontSize = this.settings.button_fontsize && this.settings.button_fontsize != "" ? this.settings.button_fontsize + 'px' : "13px";

                for (let i of this.settings.productsbtns) {
                    for (let j of i[3]) {
                        if (p === j && button.getAttribute('data-id') === p) {
                            button.setAttribute('data-title', i[1]);
                            button.innerText = i[1];
                            break
                        }
                    }
                }
                button.addEventListener('click', function() {
                    const btnId = this.getAttribute('data-id');
                    const btnIndex = this.getAttribute('data-index');
                    for (let k of stoItem) {
                        if (k.getAttribute('data-id') === btnId && k.getAttribute("data-index") === btnIndex) {
                            k.style.display = "block";
                        } else {
                            k.style.display = 'none';
                        }
                    }
                    for (let l of container.querySelectorAll(".sto-button")) {
                        l.className = 'sto-button sto-button-normal';
                    }
                    this.className = "sto-button sto-button-selected";
                    // track browse product
                    that.tracker.browse({
                        "tl": btnId,
                        "pi": btnId,
                        "pl": this.innerText
                    });
                });
                buttonWrapper.appendChild(button);
            }
            // initial
            for (let l of container.querySelectorAll(".sto-button")) {
                l.className = 'sto-button sto-button-normal';
            }
            container.querySelectorAll(".sto-button")[0].className = "sto-button sto-button-selected";
        }
    }


    buttonsStyle() {
        // set button style
        var style = document.createElement('style');
        style.innerHTML = `
            .sto-button {
                background-color: ${this.settings["color-bg_sto-button"]};
                color: ${this.settings["color-txt_sto-button"]};
            }
            .sto-button:hover {
                background-color: ${this.settings["color-bg_sto-button-selected"]};
                color: ${this.settings["color-txt_sto-button-selected"]};
            }
            .sto-button-selected {
                background-color: ${this.settings["color-bg_sto-button-selected"]};
                color: ${this.settings["color-txt_sto-button-selected"]};
            }
            `;
        document.head.appendChild(style);
    }
}

export default Buttons;
