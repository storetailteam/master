import format from '../../../index.js';

class formatTR extends format {
    constructor() {
        super();
        this.type = 'TR';
        this.html;
    }

    init(settings) {

        let $ = window.jQuery;
        let sto = window.__sto;
        let format = settings.format;
        let creaid = settings.cid;
        let custom_trackers = settings.custom_trackers;
        let lookForFormat = setInterval(function() {
            if ($('.sto-format').length > 0) {
                if (custom_trackers.length > 0) {
                    for (let i = 0; i < custom_trackers.length; i++) {

                        if (custom_trackers[i][1]) {

                            if (custom_trackers[i][0] == 'imp') {
                                let custom_track = $(custom_trackers[i][1].replace('__BUSTER__', Date.now()))
                                    .addClass('sto-custom-tracker')
                                    .attr('sto-format', format)
                                    .attr('sto-type', 'imp')
                                    .attr('sto-retail', settings.retailer);
                                $('body').append(custom_track);
                            }
                        }

                    }

                    $('body').on('mouseenter', '.sto-arrow-right, .sto-arrow-left, .sto-product-container a.next_prev, .sto-format', function(e) {
                        e.preventDefault();
                        e.stopPropagation();

                        for (let i = 0; i < custom_trackers.length; i++) {
                            if (custom_trackers[i][0] == 'click') {
                                let randomString = Math.floor(Math.random() * 100000 + 1);
                                let custom_track = $(custom_trackers[i][1].replace('__BUSTER__', Date.now()))
                                    .addClass('sto-custom-tracker_' + randomString)
                                    .attr('sto-format', format)
                                    .attr('sto-type', 'action')
                                    .attr('sto-retail', settings.retailer);
                                $('body').append(custom_track);
                            }
                        }
                    });

                }
                clearInterval(lookForFormat);
            }
        }, 200);

        setTimeout(function() {
            clearInterval(lookForFormat);
        }, 10000);


    }

}

export default formatTR;
