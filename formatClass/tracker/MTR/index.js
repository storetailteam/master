import format from '../../../index.js';
import main from './main.html';

class formatMTR extends format {
    constructor() {
        super();
        this.type = 'MTR';
        this.html;
    }

    init(settings) {
        let format = settings.format;
        let trackerList = settings.trackerList;
        let fid = settings.name + '_' + settings.cid;
        let noexid = settings.name + '_' + settings.cid;

        window.__sto[fid] = {
            'params': {},
            'click': function () {
                this.track('clk');

                let r = this.params['clk'] ? unescape(this.params['clk'][0]) + escape(settings.redirect) : settings.redirect,
                    w = window.open(r, settings.target);
                w.focus();
            },
            'track': function (tracker) {
                let t = 'https://' + settings.trackurl,
                    i = new Image();

                i.src = t + '/im?ta=' + tracker + '&rn=' + settings.retailer + '&tb=' + settings.bid + '&to=' + settings.oid + '&ti=' + settings.iid + '&tc=' + settings.cid + '&' + (Math.random() + 1).toString(36).substr(2, 10)+ '&n='+ settings.trackurl;

                // Handle extra tracking parameters
                if (this.params[tracker]) {
                    this.params[tracker].forEach(function (e, i) {
                        if (tracker !== 'clk' || i > 0) {
                            let i = new Image();
                            i.src = unescape(e);
                        }
                    });
                }
            }
        };

        window.__sto[noexid] = {
            'params': {},
            'track': function (tracker) {
                let t = 'https://' + settings.trackurl,
                    i = new Image();
                i.src = t + '/im?ta=' + tracker + '&rn=' + settings.retailer + '&tb=' + settings.main_bid + '&to=' + settings.main_oid + '&ti=' + settings.main_iid + '&tc=' + settings.main_cid + '&ci=' + window.__sto[fid].params.ci + '&' + (Math.random() + 1).toString(36).substr(2, 10)+ '&n='+ settings.trackurl;

                // Handle extra tracking parameters
                if (this.params[tracker]) {
                    this.params[tracker].forEach(function (e, i) {
                        if (tracker !== 'clk' || i > 0) {
                            let i = new Image();
                            i.src = unescape(e);
                        }
                    });
                }
            }
        };

        if (trackerList && trackerList != '') {
            window.__sto[fid].track('imp');

            trackerList.forEach(function (v, i) {

                // Define function utils
                let type = v[0];
                let url = v[1];
                let e;
                if (type === 'script') {

                    // Do image stuff
                    e = document.createElement('script');
                    e.type = 'text/javascript';
                } else {

                    // Do script stuff
                    e = document.createElement('img');
                    e.width = e.height = '0';
                    e.style.cssText = 'position:absolute; visibility:hidden;';
                }

                e.setAttribute('type','text/javascript');
                e.setAttribute('src', 'index.js?' + url.replace(/__BUSTER__/, Math.floor(Math.random() * 100000 + 1)));
                e.id = settings.cid + '_' + i;
                e.classList.add(settings.cid);
                e.setAttribute('sto-creaId', settings.cid);
                document.body.appendChild(e);
            });
        }
    }
}



export default formatMTR;
