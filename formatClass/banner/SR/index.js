import format from '../../../index.js';
import slider from './abk/slider.html';
import buttons from './abk/buttons.html';
import main from './main-redirection.html';

class formatSR extends format {
    constructor() {
        super();
        this.type = 'SR';
        this.html;
    }
    init(settings) {
        super.init(this.settings);
        this.formatType(this.settings.type, this.html);
        this.galeryFactory(this.settings);

    }
    formatType(type, html) {
        switch (type) {
            case 'abk':
                if (this.settings.frame === 'slider') {
                    let parentNode = html.querySelector('.sto-format').parentNode;
                    parentNode.setAttribute("data-frame","slider");
                    this.html.querySelector('.sto-format').remove();
                    let parser = new DOMParser();
                    let htmlTreatment = parser.parseFromString(slider, 'text/html');
                    let sliderExport = htmlTreatment.querySelector('.sto-format');
                    parentNode.append(sliderExport);
                    // parentNode.insertBefore(sliderExport, target);
                    this.html = html;
                } else if (this.settings.frame === "buttons") {
                    let target = html.querySelector(".sto-format").parentNode;
                    target.setAttribute("data-frame","btn");
                    this.html.querySelector('.sto-format').remove();
                    let parser = new DOMParser();
                    let htmlTreatment = parser.parseFromString(buttons, 'text/html');
                    let buttonsExport = htmlTreatment.querySelector('.sto-format');
                    target.append(buttonsExport);
                    let bgRight = document.createElement('div');
                    bgRight.className = 'sto-bg-right';
                    target.prepend(bgRight);
                    this.html = html;
                }
                target = html.querySelector(".sto-format").parentNode;
                if (this.settings.option != '' && this.settings.option !== undefined) {
                    target.setAttribute("data-opt","1");
                } else {
                    target.setAttribute("data-opt","0");
                }
                break;
            case 'first':
                break;
            case 'cross':
                htmlType.className = 'sto-product-container';
                html.append(htmlType);
                return htmlType;
                break;
            case 'redirection':
                let containerMain = html.querySelector('.sto-format');
                containerMain.innerHTML = main;
                let target = html.querySelector('.sto-logo');
                let parentNode = html.querySelector('.sto-format');
                this.html = html;
            default:
        }
    }

    checkDataSize(container) {

        let that = this;
        let width;
        let _container = document.querySelector('.sto-__TAG__-container');

        window.addEventListener('resize', function() {

            this.setTimeout(function() {
                width = container.querySelector('.sto-claim').offsetWidth;
                container.querySelector('.sto-claim').className = 'sto-claim sto-claim-' + width;
                that.imageAssign();
            },100);
        }),
        window.setTimeout(function() {
            var t = window.document.createEvent('UIEvents');
            t.initUIEvent('resize', !0, !1, window, 0),
            window.dispatchEvent(t);
        });

    }


    builder(galery) {
        let formatBuilder = {
            'tag': 'div',
            'class': 'sto-container',
            'galery': galery,
        };

        return formatBuilder;
    }
}
export default formatSR;
