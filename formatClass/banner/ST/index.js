import format from '../../../index.js';
class formatST extends format {
    constructor() {
        super();
        this.type = 'ST';
        this.html;
    }
    init(settings) {
        super.init(this.settings);
        this.formatType(this.settings.type, this.html);
        this.galeryFactory(this.settings);

        // this.redirect("sto-format");
    }
    formatType(type, html) {
        switch (type) {
            case 'abk':
                if (this.settings.frame === 'slider') {
                    let target = html.querySelector('.sto-logo2');
                    let parentNode = html.querySelector('.sto-format');
                    let parser = new DOMParser();
                    let htmlTreatment = parser.parseFromString(slider, 'text/html');
                    let sliderExport = htmlTreatment.querySelector('.sto-carrousel');
                    parentNode.insertBefore(sliderExport, target);
                    this.html = html;
                } else if (this.settings.frame === 'buttons') {
                    let target = html.querySelector('.sto-format').parentNode;
                    this.html.querySelector('.sto-format').remove();
                    let parser = new DOMParser();
                    let htmlTreatment = parser.parseFromString(buttons, 'text/html');
                    let buttonsExport = htmlTreatment.querySelector('.sto-format');
                    target.append(buttonsExport);
                    let bgRight = document.createElement('div');
                    bgRight.className = 'sto-bg-right';
                    target.prepend(bgRight);
                    this.html = html;
                }
                break;
            case 'first':

                break;
            case 'cross':
                htmlType.className = 'sto-product-container';
                html.append(htmlType);
                return htmlType;
                break;
            default:
        }
    }
    builder(galery) {
        let formatBuilder = {
            'tag': 'div',
            'class': 'sto-container',
            'galery': galery,
        };

        // formatBuilder.galery.push(galery)
        return formatBuilder;
    }
}
export default formatST;
