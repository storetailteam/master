import format from '../../../index.js';
import megasky from './abk/main.html';
class formatBBO extends format {
    constructor() {
        super();
        this.type = "BBO";
        this.html;
    }
    init(settings) {
        super.init(this.settings);
        this.formatType(this.settings.type, this.html);
        this.galeryFactory(this.settings);
    }
    formatType(type, html) {
        switch (type) {
            case "redirection":
                    let target = html.querySelector(".sto-format").parentNode;
                    target.querySelector('.sto-format').remove();
                    let parser = new DOMParser();
                    let htmlTreatment = parser.parseFromString(megasky, "text/html");
                    let mskExport = htmlTreatment.querySelector(".sto-format");
                    target.append(mskExport);                    
                    this.html = html;
                break;
            case "first":

                break;
            case "cross":
                htmlType.className = "sto-product-container";
                html.append(htmlType);
                return htmlType;
                break;
            default:
        }
    }
    builder(galery) {
        let formatBuilder = {
            "tag": "div",
            "class": "sto-container",
            "galery": galery,
        };
        return formatBuilder;
    }
}
export default formatBBO;
