import format from '../../../index.js';
import slider from '../abk/slider.html';
import buttons from '../abk/buttons.html';
class formatIM extends format {
    constructor() {
        super();
        this.type = 'IM';
        this.html;
    }
    init(settings) {
        super.init(this.settings);
        this.formatType(this.settings.type, this.html);
        this.galeryFactory(this.settings);

        // this.redirect("sto-format");
    }
    formatType(type, html) {
        switch (type) {
            case 'abk':
                if (this.settings.frame === 'slider') {
                    let target = html.querySelector('.sto-logo2');
                    let parentNode = html.querySelector('.sto-format');
                    let parser = new DOMParser();
                    let htmlTreatment = parser.parseFromString(slider, 'text/html');
                    let sliderExport = htmlTreatment.querySelector('.sto-carrousel');
                    parentNode.insertBefore(sliderExport, target);
                    this.html = html;
                } else if (this.settings.frame === 'buttons') {
                    let target = html.querySelector('.sto-logo2');
                    let parentNode = html.querySelector('.sto-format');
                    let parser = new DOMParser();
                    let htmlTreatment = parser.parseFromString(buttons, 'text/html');
                    let buttonsExport = htmlTreatment.querySelector('.sto-carrousel');
                    parentNode.insertBefore(buttonsExport, target);
                    this.html = html;
                }
                break;
            case 'first':

                break;
            case 'cross':
                htmlType.className = 'sto-product-container';
                html.append(htmlType);
                return htmlType;
                break;
            default:
        }
    }
    builder(galery) {
        let formatBuilder = {
            'tag': 'div',
            'class': 'sto-container',
            'galery': galery,
        };

        // formatBuilder.galery.push(galery)
        return formatBuilder;
    }
    impulseInteractions(container, impWrapper) {
        let that = this;
        impWrapper = impWrapper ? impWrapper : container ;
        impWrapper.setAttribute('data-state','open');
        container.querySelector('.sto-close-button').addEventListener('click', function() {
            that.trackFormat('closeFormat', false);
            impWrapper.setAttribute('data-state','close');
            setTimeout(function() {
                impWrapper.parentNode.removeChild(impWrapper);
            }, 1500);
        });
    }

    checkDataSize(container) {

        let that = this;
        let width;
        let _container = document.querySelector('.sto-__TAG__-container');

        window.addEventListener('resize', function() {

            this.setTimeout(function() {
                width = container.querySelector('.sto-claim').offsetWidth;
                container.querySelector('.sto-claim').className = 'sto-claim sto-claim-' + width;
                that.imageAssign();
            },100);
        });

    }

}
export default formatIM;
