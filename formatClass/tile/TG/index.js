import format from '../../../index.js';
import tile from '../abk/prod.html';

class formatBF extends format {
    constructor() {
        super();
        this.type = 'TG';
        this.html;
    }
    init(settings) {
        super.init(settings);
        this.galeryFactory(settings);
        this.formatBuilder = this.builder(this.galery);
        this.formatType(this.settings.type, this.html);
    }
    formatType(type, html) {
        switch (type) {
            case 'abk':
                if (this.settings.frame === 'tile') {
                    let target = html.querySelector('.sto-format');
                    let parentNode = html.querySelector('.sto-container');
                    let parser = new DOMParser();
                    let htmlTreatment = parser.parseFromString(tile, 'text/html');
                    let tileExport = htmlTreatment.querySelector('.sto-product-container');
                    target.innerHTML = tile;
                    this.html = html;
                }
                break;
            case 'first':

                break;
            case 'cross':

                // let htmlType = document.createElement("div");
                htmlType.className = 'sto-product-container';
                html.append(htmlType);
                return htmlType;
                break;
            default:
        }
    }
    builder(galery) {
        let formatBuilder = {
            'tag': 'div',
            'class': 'sto-container',
            'galery': galery
        };
        return formatBuilder;
    }
    btfButtonsDebug(prods, container) {

        let that = this,
            buttonWrapper = document.createElement('div'),
            productExiste = 0,
            justHover,
            btns = {};


        buttonWrapper.className = 'sto-buttons-wrapper';

        if (that.settings['img_sto-button-normal'] && that.settings['img_sto-button-normal'] != '') {
            buttonWrapper.setAttribute('button-normal', 'img');
        } else {
            buttonWrapper.setAttribute('button-normal', 'color');
        }
        if (that.settings['img_sto-button-selected'] && that.settings['img_sto-button-selected'] != '') {
            buttonWrapper.setAttribute('button-selected', 'img');
        } else {
            buttonWrapper.setAttribute('button-selected', 'color');
        }
        container.querySelector('.sto-image-container').appendChild(buttonWrapper);

        for (let index in prods) {
            let realProducts;
            productExiste = 0;
            if (index < container.querySelectorAll('.sto-product-container>*').length) {
                realProducts = container.querySelectorAll('.sto-product-container>*')[index].getAttribute('data-id');
            } else {
                realProducts = '';
            }
            for (let sub in prods[index][3]) {

                if (container.querySelectorAll('.sto-product-container>*[data-id=\''+prods[index][3][sub]+'\']') && productExiste < 1) {
                    let button = document.createElement('div');
                    button.className = 'sto-button sto-button-normal';
                    button.setAttribute('data-index', index);
                    button.setAttribute('data-id', prods[index][3][sub]);
                    button.setAttribute('data-title', prods[index][1]);
                    button.style.width = that.settings.button_width && that.settings.button_width != '' ? this.settings.button_width + 'px' : '150px';
                    button.style.fontSize = that.settings.button_fontsize && that.settings.button_fontsize != '' ? that.settings.button_fontsize + 'px' : '13px';
                    button.innerHTML = '<span>'+prods[index][1]+'</span>';
                    btns.cid = that.settings.cid;
                    btns.buttons = button;

                    justHover = false;
                    button.addEventListener('mouseenter', function() {
                        if (this.className.indexOf('sto-button-selected') == -1) {
                            this.className = 'sto-button sto-button-selected';
                            that.imageAssign();
                            that.colorAssign();
                            justHover = true;
                        }
                    });
                    button.addEventListener('mouseleave', function() {
                        if (justHover == true && this.className.indexOf('sto-button-selected') > -1) {
                            this.className = 'sto-button sto-button-normal';
                            that.imageAssign();
                            that.colorAssign();
                            justHover = false;
                        }
                    });
                    button.addEventListener('click', function() {

                        for (let k = 0; k < container.querySelectorAll('.sto-product-container>*').length; k++) {
                            container.querySelectorAll('.sto-product-container>*')[k].style.display = 'none';
                        }

                        container.querySelector('.sto-product-container>*[data-id="' + prods[index][3][sub] + '"]').style.display = 'inline-block';
                        for (let l = 0; l < container.querySelectorAll('div.sto-button').length; l++) {
                            container.querySelectorAll('div.sto-button')[l].className = 'sto-button sto-button-normal';
                        }
                        this.className = 'sto-button sto-button-selected';
                        that.trackFormat(
                            'trackBrowse', {
                                id: this.getAttribute('data-id'),
                                title: this.getAttribute('data-title')
                            }
                        );

                        that.imageAssign();
                        that.colorAssign();

                        justHover = false;
                    });

                    if (container.className.indexOf(btns.cid) > -1) {
                        container.querySelector('.sto-buttons-wrapper').appendChild(btns.buttons);
                    }

                    productExiste++;

                }
            }
            if (index >= 4) {
                break;
            }

        }

        container.querySelectorAll('div.sto-button')[0].className = 'sto-button sto-button-selected';

        return container;
    }
    btfButtons(crawl, prods, container) {
        let that = this,
            i, j, k, l,
            justHover,
            buttonWrapper = document.createElement('div'),
            firstAvailProd = 0;

        buttonWrapper.className = 'sto-buttons-wrapper';
        if (that.settings['img_sto-button-normal'] && that.settings['img_sto-button-normal'] != '') {
            buttonWrapper.setAttribute('button-normal', 'img');
        } else {
            buttonWrapper.setAttribute('button-normal', 'color');
        }
        if (that.settings['img_sto-button-selected'] && that.settings['img_sto-button-selected'] != '') {
            buttonWrapper.setAttribute('button-selected', 'img');
        } else {
            buttonWrapper.setAttribute('button-selected', 'color');
        }

        for (i = 0; i < prods.length; i++) {

            firstAvailProd = 0;
            for (j = 0; j < prods[i][3].length; j++) {

                if (crawl[prods[i][3][j]] && firstAvailProd == 0) {

                    firstAvailProd++;
                    let button = document.createElement('div');
                    button.className = 'sto-button sto-button-normal';
                    button.setAttribute('data-index', i);
                    button.setAttribute('data-id', prods[i][3][j]);
                    button.setAttribute('data-title', prods[i][1]);
                    button.style.width = this.settings.button_width && this.settings.button_width != '' ? this.settings.button_width + 'px' : '150px';
                    button.style.fontSize = this.settings.button_fontsize && this.settings.button_fontsize != '' ? this.settings.button_fontsize + 'px' : '13px';
                    button.innerHTML = '<span>'+prods[i][1]+'</span>';
                    justHover = false;

                    button.addEventListener('mouseenter', function() {
                        if (this.className.indexOf('sto-button-selected') == -1) {
                            this.className = 'sto-button sto-button-selected';
                            that.imageAssign();
                            that.colorAssign();
                            justHover = true;
                        }
                    });

                    button.addEventListener('mouseleave', function() {
                        if (justHover == true && this.className.indexOf('sto-button-selected') > -1) {
                            this.className = 'sto-button sto-button-normal';
                            that.imageAssign();
                            that.colorAssign();
                            justHover = false;
                        }
                    });

                    button.addEventListener('click', function() {

                        for (k = 0; k < document.querySelectorAll('.sto-product-container>*').length; k++) {
                            document.querySelectorAll('.sto-product-container>*')[k].style.display = 'none';
                        }

                        document.querySelector('.sto-product-container>*[data-id="' + this.getAttribute('data-id') + '"]').style.display = 'inline-block';
                        for (l = 0; l < document.querySelectorAll('div.sto-button').length; l++) {
                            document.querySelectorAll('div.sto-button')[l].className = 'sto-button sto-button-normal';
                        }

                        this.className = 'sto-button sto-button-selected';
                        that.trackFormat(
                            'trackBrowse', {
                                id: this.getAttribute('data-id'),
                                title: this.getAttribute('data-title')
                            }
                        );

                        that.imageAssign();
                        that.colorAssign();

                        justHover = false;
                    });

                    buttonWrapper.appendChild(button);
                    firstAvailProd++;
                }

            }
            if (i >= 4) {
                break;
            }
        }

        buttonWrapper.querySelectorAll('div.sto-button')[0].className = 'sto-button sto-button-selected';

        container.querySelector('.sto-image-container').appendChild(buttonWrapper);

        that.optionsSetup(container);

        that.imageAssign();
        that.colorAssign();
        return container;
    }

}

export default formatBF;
