import treatmentHTML from './main.html';
import tools from 'tools';
import logger from '../../../../tools/logger.js';

class formatSC {

    constructor(mobileBreakpoint = false) {
        this.type = 'SC';
        this.mobileBreakpoint = mobileBreakpoint;
        this.tools = new tools;
    }

    init(settings, tracker) {
        this.ingestSettings(settings);
        this.divID = this.getDivID();
        this.tracker = tracker;

        this.loadHTMl();
        this.applyImages();
        this.applyColors();
        this.optionsSetup();
        this.wordingSetup();

        if (this.mobileBreakpoint) {
            this.setBreakpoints();

            // switch option container placment when switching between desktop and mobile
            this.handleOptionContainerTransition();
        }
    }

    // assigns settings to this object, builds a list of skus for use in createFormat
    ingestSettings(settings) {
        let skus = [];
        if (settings.productsbtns && settings.productsbtns.length > 0) {
            settings.productsbtns.forEach((productbtn) => {

                // productbtns are arrays that have skus in position 3
                if (productbtn[3]) {
                    skus = skus.concat(productbtn[3]);
                }
            });
        } else {
            logger.warn('Delivery response does not contain any products - settings.productsbtns is empty or missing');
        }
        settings.products = skus;
        this.settings = settings;
    }

    loadHTMl() {
        const parser = new DOMParser();
        const treatment = parser.parseFromString(treatmentHTML, 'text/html');
        const outerDiv = treatment.querySelector('.sto-container');
        outerDiv.id = this.divID;
        outerDiv.setAttribute('data-type', this.type);
        outerDiv.setAttribute('data-creaid', this.settings.cid);
        this.html = outerDiv;
    }

    getDivID() {
        return `sto-container-${this.type}-${this.settings.cid}`;
    }

    applyImages() {
        const attributeMapping =
            {
                'img_sto-image-container': '.sto-image-web',
                'img_sto-image-mobile': '.sto-image-mobile',
                'img_sto-option': '.sto-option',
                'img_sto-option2': '.sto-option2'
            };
        for (const setting in attributeMapping) {
            if (this.settings[setting]) {
                const target = this.html.querySelector(attributeMapping[setting]);
                target.style.backgroundImage = `url(${this.settings.path}${this.settings[setting]})`;
            }
        }
    }

    applyColors() {
        const attributeMapping =
            {
                'color-bg_sto-image-container': {selectors: '.sto-image-web, .sto-mobile-options', style: 'backgroundColor'},
                'color-bg_sto-option': {selectors: '.sto-option', style: 'backgroundColor'},
                'color-bg_sto-option2': {selectors: '.sto-option2', style: 'backgroundColor'},
                'color-bg_sto-wording': {selectors: '.sto-wording', style: 'backgroundColor'},
                'color-border_sto-format': {selectors: '.sto-wording', style: 'borderColor'},
                'color-txt_sto-option-text': {selectors: '.sto-option', style: 'color'},
                'color-txt_sto-option-text2': {selectors: '.sto-option2', style: 'color'},
                'color-txt_sto-wording': {selectors: '.sto-wording', style: 'color'},
            };
        for (const setting in attributeMapping) {
            if (this.settings[setting]) {
                const targets = this.html.querySelectorAll(attributeMapping[setting]['selectors']);
                for (let i = 0; i < targets.length; i++) {
                    targets[i].style[attributeMapping[setting]['style']] = this.settings[setting];
                }
            }
        }
    }

    wordingSetup() {
        this.html.querySelector('.sto-wording').innerHTML = this.settings.wording;
    }

    optionsSetup() {
        if (this.settings.option === 'redirection') {

            // the options button is a link that redirects to a brand page
            const attributeMapping =
                {
                    '.sto-option': {text: 'sto_option_text', redirect: 'redirection_url', target: 'redirection_target'},
                    '.sto-option2': {text: 'sto_option_text2', redirect: 'redirection_url2', target: 'redirection_target'}
                };

            for (const selector in attributeMapping) {
                const optionContainer = this.html.querySelector(selector);
                const text = this.settings[attributeMapping[selector].text];
                const redirect = this.settings[attributeMapping[selector].redirect];
                const target = this.settings[attributeMapping[selector].target];
                if (text && redirect && target) {
                    optionContainer.innerHTML = text;
                    optionContainer.addEventListener('click', this.openOptionRedirect.bind(this, redirect, target));
                    optionContainer.setAttribute('data-populated', 'true');
                } else {
                    logger.warn(`Option values in settings are missing either text, redirect or target for ${selector}`);
                }
            }
        } else if (this.settings.option === 'video') {

            // the options button opens a popup with a video
            const attributeMapping =
                {
                    '.sto-option': {text: 'sto_option_text', video_iframe: 'video_iframe'},
                    '.sto-option2': {text: 'sto_option_text2', video_iframe: 'video_iframe2'}
                };

            for (const selector in attributeMapping) {
                const optionContainer = this.html.querySelector(selector);
                const text = this.settings[attributeMapping[selector].text];
                const video_iframe = this.settings[attributeMapping[selector].video_iframe];
                if (text && video_iframe) {
                    optionContainer.innerHTML = text;
                    optionContainer.addEventListener('click', this.loadVideoIframe.bind(this));
                    optionContainer.setAttribute('data-populated', 'true');
                } else {
                    logger.warn(`Option values in settings are missing either text or video_iframe for ${selector}`);
                }
            }
        } else {
            logger.error('We have a value for settings.option that isnt handled in options setup');
        }
    }

    openOptionRedirect(redirect, target) {
        if (target === '_self') {

            // open link in same tab
            window.location.href = redirect;
        } else if (target === '_blank') {

            // open link in different tab
            window.open(redirect, '_blank');
        }
        this.tracker.click();
    }

    loadVideoIframe() {
        if (!document.querySelector('body .sto-video')) {
            this.tracker.playVideo();
            const body = document.querySelector('body'),
                video = document.createElement('div'),
                videoBg = document.createElement('div');
            video.className = 'sto-video';
            video.innerHTML = '<div class="sto-close-video"><div class="sto-picto-close"></div></div><div class="sto-video-wrapper">' + this.settings.video_iframe + '</div>';
            videoBg.className = 'sto-video-background';
            body.appendChild(videoBg);
            body.appendChild(video);
            videoBg.addEventListener('click', this.removeVideoIframe.bind(this, body, video, videoBg));
            video.addEventListener('click', this.removeVideoIframe.bind(this, body, video, videoBg));
        }
    }

    removeVideoIframe(body, video, videoBg) {
        this.tracker.closeVideo();
        body.removeChild(video);
        body.removeChild(videoBg);
    }

    setBreakpoints() {
        this.setDataViewport();
        window.addEventListener('resize', this.setDataViewport.bind(this));
    }

    setDataViewport() {
        if (this.isInMobileView()) {
            this.html.setAttribute('data-viewport', 'mobile');
        } else {
            this.html.setAttribute('data-viewport', 'desktop');
        }
    }

    handleOptionContainerTransition() {
        this.positionOptionDivs();
        window.addEventListener('resize', this.positionOptionDivs.bind(this));
    }

    positionOptionDivs() {
        const imgContainer = this.html.querySelector('.sto-image-container');
        const mobileOptionsContainer = this.html.querySelector('.sto-mobile-options');
        const option1 = this.html.querySelector('.sto-option');
        const option2 = this.html.querySelector('.sto-option2');
        if (this.isInMobileView()) {
            if (mobileOptionsContainer.innerHTML === '') {
                mobileOptionsContainer.appendChild(option1);
                mobileOptionsContainer.appendChild(option2);
            }
        } else {
            if (mobileOptionsContainer.innerHTML !== '') {
                imgContainer.appendChild(option1);
                imgContainer.appendChild(option2);
            }
        }
    }

    isInMobileView() {
        return window.matchMedia('(max-width: ' + this.mobileBreakpoint + 'px)').matches;
    }

    fireImpTracking() {
        if (document.querySelector(`#${this.divID}`)) {
            this.tracker.display();
        } else {
            logger.error(`Failed to fire impression tracker, div #${this.divID} not found`);
        }
    }

    fireClickTracking(redirectURL, target) {
        this.tracker.click();
        window.open(redirectURL, target);
    }
}

export default formatSC;