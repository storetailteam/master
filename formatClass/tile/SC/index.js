import format from '../../../index.js';
import tile from './abk/main.html';
class formatSC extends format {
    constructor() {
        super();
        this.type = 'SC';
        this.html;
        this.void = '';
    }
    init(settings) {
        super.init(settings);
        this.galeryFactory(settings);
        this.formatBuilder = this.builder(this.galery);
        __TYPE__ !== 'api' ? this.formatType(this.settings.type, this.html) : this.void;
        this.imageAssign();
        this.colorAssign();

        //this.redirect("sto-format");
    }
    formatType(type, html) {
        switch (type) {
            case 'abk':
                if (this.settings.frame === 'tile') {
                    let target = html.querySelector('.sto-format');
                    let parentNode = html.querySelector('.sto-container');
                    let parser = new DOMParser();
                    let htmlTreatment = parser.parseFromString(tile, 'text/html');
                    let tileExport = htmlTreatment.querySelector('.sto-product-container');
                    target.innerHTML = tile;
                    this.html = html;
                }
                break;
            case 'first':

                break;
            case 'cross':

                // let htmlType = document.createElement("div");
                htmlType.className = 'sto-product-container';
                html.append(htmlType);
                return htmlType;
                break;
            default:
        }
    }
    builder(galery) {
        let formatBuilder = {
            'tag': 'div',
            'class': 'sto-container',
            'galery': galery
        };
        return formatBuilder;
    }
    backupCheck(crawl, container, parentw, tilew) {

        let productsPremium = [],
            prodsPremIndex = this.settings.productsbtns,
            productsBackup = this.settings.products_backup,
            premiumCounter = 0,
            backupCounter = 0,
            finalProds = [],
            capacity = Math.floor(parentw / tilew);

        for (let i = 0; i < prodsPremIndex.length; i++) {
            productsPremium = productsPremium.concat(prodsPremIndex[i][3]);
        }

        productsPremium.forEach(function(id) {
            if (crawl[id]) premiumCounter++;
        });

        if (premiumCounter < (capacity - 1)) {
            productsBackup.forEach(function(id) {
                if (crawl[id]) backupCounter++;
            });
            if (backupCounter + premiumCounter >= capacity) {
                finalProds = productsPremium.concat(productsBackup);
                container.className += ' scbackup';
            }
        } else {
            finalProds = productsPremium;
        }
        return [finalProds, container];
    }
    viewportCheck(container, breakpoint) {
        let control;
        setInterval(function() {

            // set data viewport
            if (container.offsetWidth !== control) {
                if (window.matchMedia('(max-width: ' + breakpoint + 'px)').matches) {
                    container.setAttribute('data-viewport', 'mobile');
                } else {
                    container.setAttribute('data-viewport', 'desktop');
                }
                control = container.offsetWidth;
            }
        });
    }
    nbProductCheck(crawl, products, nbProductMin) {
        let nbAvailableProds = 0;
        for (let i in crawl) {
            if (products.indexOf(i) > -1) {
                nbAvailableProds++;
            }
        }
        if (nbAvailableProds < nbProductMin) {
            throw new Error('Not enough products');
        }
    }

}

export default formatSC;
