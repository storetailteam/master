import format from '../../../index.js';
import tile from './abk/tile.html';

class formatVI extends format {
    constructor() {
        super();
        this.type = "VCR";
    }
    init(settings) {
        super.init(settings);
        this.formatType(this.settings.type, this.html);
        this.galeryFactory(settings);
        this.formatBuilder = this.builder(this.galery);
        this.imageAssign();
        this.colorAssign();
    }
    builder(galery) {
        let formatBuilder = {
            "tag": "div",
            "class": "sto-" + __TYPE__ + "-container",
            "galery": galery
        };
        return formatBuilder;
    }
    formatType(type, html) {
        html = this.viClaim(html, this.settings.vignette_type);
        html.setAttribute('data-frame', this.settings.vignette_type);
        switch (type) {
            case "abk":
                if (this.settings.frame === "tile") {
                    let parser = new DOMParser();
                    let htmlTreatment = parser.parseFromString(tile, "text/html");
                    let tileExport = htmlTreatment.querySelector(".sto-vignette");
                    html.querySelector('.sto-format').appendChild(tileExport);
                    this.html = html;
                }
                break;
            case 'cross':
                htmlType.className = 'sto-product-container';
                html.append(htmlType);
                return htmlType;
                break;
            default:
        }
    }
    tileHeigh(container, h) {
        let that = this,
            elHeight;
        window.addEventListener('resize', function() {
            if (container.getAttribute('data-viewport') === 'desktop') {
                elHeight = h.offsetHeight;
                elHeight += parseInt(window.getComputedStyle(h).getPropertyValue('margin-top'));
                container.style.height = elHeight + 'px';
            }
        });
    }
    viClaim(html, viType) {
        let that = this;
        let viHeader = document.createElement('div');
        viHeader.className = 'sto-option';
        viHeader.innerHTML = '</div></div><div class="sto-option-wrapper"><span class="sto-option-text">' + that.settings.option_text + '</span></div>';
        html.appendChild(viHeader);
        return html;
    }
}

export default formatVI;
