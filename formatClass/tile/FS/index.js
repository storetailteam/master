import format from '../../../index.js';
import tile from '../abk/prod.html';
class formatFS extends format {
    constructor() {
        super();
        this.type = 'FS';
        this.html;
    }
    init(settings) {
        super.init(settings);
        this.galeryFactory(settings);
        this.formatBuilder = this.builder(this.galery);
        this.formatType(this.settings.type, this.html);
        this.imageAssign();
        this.colorAssign();
        this.formatSize(this.settings.format_size, this.html);
        this.imageSize(this.html);
    }
    formatType(type, html) {
        const that = this;
        switch (type) {
            case 'abk':
                if (this.settings.frame === 'tile') {
                    let target = html.querySelector('.sto-format');
                    let parentNode = html.querySelector('.sto-container');
                    let parser = new DOMParser();
                    let htmlTreatment = parser.parseFromString(tile, 'text/html');
                    let tileExport = htmlTreatment.querySelector('.sto-product-container');
                    let arrowL = '<div class=\'sto-arrows sto-arrow-left\'></div>',
                        arrowR = '<div class=\'sto-arrows sto-arrow-right\'></div>';

                    target.innerHTML = tile;
                    html.querySelector('.sto-product-container').innerHTML += arrowL;
                    html.querySelector('.sto-product-container').innerHTML += arrowR;

                    this.html = html;

                }
                break;
            case 'first':

                break;
            case 'cross':
                htmlType.className = 'sto-product-container';
                html.append(htmlType);
                return htmlType;
                break;
            default:
        }
    }
    builder(galery) {
        let formatBuilder = {
            'tag': 'div',
            'class': 'sto-container',
            'galery': galery
        };
        return formatBuilder;
    }
    backupCheck(crawl, container, parentw, tilew) {

        let productsPremium = this.settings.products,
            productsBackup = this.settings.products_backup,
            premiumCounter = 0,
            backupCounter = 0,
            finalProds = [],
            capacity = Math.floor(parentw / tilew);

        productsPremium.forEach(function(id) {
            if (crawl[id]) premiumCounter++;
        });

        if (premiumCounter < (capacity - 1)) {
            productsBackup.forEach(function(id) {
                if (crawl[id]) backupCounter++;
            });
            if (backupCounter + premiumCounter >= capacity) {
                finalProds = productsPremium.concat(productsBackup);
                container.className += ' scbackup';
            }
        } else {
            finalProds = productsPremium;
        }
        return [finalProds, container];
    }
    imageSize(target) {
        const that = this;
        this.imageGestion(target);
        window.addEventListener('resize', function() {
            that.imageGestion(target);
        });
    }
    imageGestion(target) {
        const that = this;
        if ((target.offsetWidth) > 1152) {
            target.querySelector('.sto-image-container').style.width = '576px';
            target.querySelector('.sto-product-container').style.width = 'calc(100% - 576px)';

        } else if ((target.offsetWidth) < 1152 && target.getAttribute('data-viewport') === 'desktop') {
            target.querySelector('.sto-image-container').style.width = '50%';
            target.querySelector('.sto-product-container').style.width = '50%';
        }

    }
    formatSize(size, html) {
        if (size === 'medium') {
            html.setAttribute('data-size', 'medium');
        } else if (size === 'small') {
            html.setAttribute('data-size', 'small');
        }
    }
    mobileDisplay(container, breakpoint, titleClass) {
        const that = this,
            stoItems = container.querySelectorAll('.sto-product-container>*:not(.sto-arrows):not(.sto-slider-indicators)'),
            productContainer = container.querySelector('.sto-product-container'),
            nbProducts = that.settings.products.length,
            indicatorsContainer = document.createElement('div');
        let control;

        indicatorsContainer.className = 'sto-slider-indicators';
        productContainer.append(indicatorsContainer);

        // data title
        stoItems.forEach(function(e) {
            e.setAttribute('data-title', e.querySelector(titleClass).textContent);
        });

        // add class products
        if (!stoItems[0].classList.contains('sto-item-product')) {
            for (let o of stoItems) {
                o.className += ' sto-item-product';
            }
        }

        // indicators
        indicatorsContainer.innerHTML += '<ul class=\'sto-indicators-list\'></ul>';
        for (let i of stoItems) {
            indicatorsContainer.querySelector('.sto-indicators-list').innerHTML += '<li class=\'sto-indicator\'></li>';
        }
        const indicators = container.querySelectorAll('.sto-indicator');

        // set initial selected item
        stoItems[0].className += ' selected-item';
        if (stoItems.length >= 3) {
            stoItems[1].className += ' next-item';
            stoItems[stoItems.length - 1].className += ' prev-item';
        }
        indicators[0].className += ' current';
        window.addEventListener('resize', function() {
            let productContainerWidth = container.querySelector('.sto-image-container').getBoundingClientRect().width,
                selected = container.querySelector('.selected-item'),
                indexSelected,
                margin;

            // set data viewport
            if (container.offsetWidth !== control) {
                if (window.matchMedia('(max-width: ' + breakpoint + 'px)').matches) {
                    container.setAttribute('data-viewport', 'mobile');
                } else {
                    container.setAttribute('data-viewport', 'desktop');
                }
                control = container.offsetWidth;
            }

            // size product items
            if (container.getAttribute('data-viewport') === 'mobile') {
                for (let index of stoItems) {
                    index.style.maxWidth = (productContainerWidth / 2) + 'px';
                }
            } else if (container.getAttribute('data-viewport') === 'desktop') {
                for (let index of stoItems) {
                    index.style.maxWidth = '700px';
                }
            }
        });

        // carrousel
        stoItems.forEach(function(e) {
            e.addEventListener('click', function() {

                // tracker
                if (!e.classList.contains('selected-item')) {
                    that.trackFormat(
                        'trackBrowse', {
                            id: e.getAttribute('data-id'),
                            title: e.getAttribute('data-title')
                        }
                    );
                }

                // toggle class
                for (let index of stoItems) {
                    index.classList.remove('selected-item');
                    index.classList.remove('prev-item');
                    index.classList.remove('next-item');
                }
                for (let q of indicators) {
                    q.classList.remove('current');
                }
                e.className += ' selected-item';
                let indexSelected = [].indexOf.call(stoItems, e);
                indicators[indexSelected].className += ' current';
                if (stoItems.length >= 3) {

                    // prev
                    if (indexSelected === 0) {
                        stoItems[stoItems.length - 1].className += ' prev-item';
                    } else {
                        stoItems[indexSelected - 1].className += ' prev-item';
                    }

                    // next
                    if (indexSelected === (stoItems.length - 1)) {
                        stoItems[0].className += ' next-item';
                    } else {
                        stoItems[indexSelected + 1].className += ' next-item';
                    }
                }

            });
        });

        // click indicators
        indicators.forEach(function(e) {
            e.addEventListener('click', function() {

                // toggle class
                for (let r of stoItems) {
                    r.classList.remove('selected-item');
                    r.classList.remove('prev-item');
                    r.classList.remove('next-item');
                }
                for (let q of indicators) {
                    q.classList.remove('current');
                }
                e.className += ' current';
                let indexCurrent = [].indexOf.call(indicators, e);
                stoItems[indexCurrent].className += ' selected-item';
                if (stoItems.length >= 3) {

                    // prev
                    if (indexCurrent === 0) {
                        stoItems[stoItems.length - 1].className += ' prev-item';
                    } else {
                        stoItems[indexCurrent - 1].className += ' prev-item';
                    }

                    // next
                    if (indexCurrent === (stoItems.length - 1)) {
                        stoItems[0].className += ' next-item';
                    } else {
                        stoItems[indexCurrent + 1].className += ' next-item';
                    }
                }
                that.trackFormat(
                    'trackBrowse', {
                        id: this.closest('.sto-product-container').querySelector('.selected-item').getAttribute('data-id'),
                        title: this.getAttribute('data-title')
                    }
                );
            });
        });
    }
    trackerDesktop(container) {
        const that = this;
        setTimeout(function() {
            let arrows = container.querySelectorAll('.sto-arrows');
            arrows.forEach(function(e) {
                e.addEventListener('click', function() {
                    let productId = this.getAttribute('data-id');
                    that.trackFormat(
                        'trackBrowse', {
                            id: productId,
                            title: container.querySelector('.sto-item-product[data-id=\'' + productId + '\']').getAttribute('data-title')
                        }
                    );
                });
            });
        }, 200);
    }
}

export default formatFS;
