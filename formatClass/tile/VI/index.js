import format from '../../../index.js';
import tile from './abk/tile.html';

class formatVI extends format {
    constructor() {
        super();
        this.type = 'VI';
    }
    init(settings) {
        super.init(settings);
        this.formatType(this.settings.type, this.html);
        this.galeryFactory(settings);
        this.formatBuilder = this.builder(this.galery);

        // this.html.style.backgroundImage = "url("+this.galery.images.bg+")";
        this.imageAssign();
        this.colorAssign();

    //this.redirect("sto-format");
    }
    builder(galery) {
        let formatBuilder = {
            'tag': 'div',
            'class': 'sto-container',
            'galery': galery
        };
        return formatBuilder;
    }
    formatType(type, html) {

        /*
         * let child = html.querySelector('.sto-logo');
         * child.parentNode.innerHTML = '';
         */
        html = this.viClaim(html, this.settings.vignette_type);
        html.setAttribute('data-frame', this.settings.vignette_type);
        switch (type) {
            case 'abk':
                if (this.settings.frame === 'tile') {
                    let parser = new DOMParser();
                    let htmlTreatment = parser.parseFromString(tile, 'text/html');
                    let tileExport = htmlTreatment.querySelector('.sto-vignette');

                    html.querySelector('.sto-format').appendChild(tileExport);
                    this.html = html;
                }
                break;
            case 'first':

                break;
            case 'redirection':

                /*
                 * let divMobile = createElement('div');
                 * divMobile.className = 'sto-image-mobile';
                 * html.querySelector('.sto-format').appendChild(divMobile);
                 */
                html.querySelector('.sto-format').innerHTML = '<div class=\'sto-mobile-box\'></div>';
                break;
            case 'cross':

                /*
                 * htmlType.className = "sto-product-container";
                 * html.append(htmlType);
                 * return htmlType;
                 */
                break;
            default:
        }
    }
    tileHeigh(container, h) {
        let that = this,
            elHeight;
        window.addEventListener('resize', function() {
            if (container.getAttribute('data-viewport') === 'desktop') {
                elHeight = h.offsetHeight;
                elHeight += parseInt(window.getComputedStyle(h).getPropertyValue('margin-top'));

                //elHeight += parseInt(window.getComputedStyle(h).getPropertyValue('margin-bottom'));
                container.style.height = elHeight + 'px';
            }
        });
    }
    viClaim(html, viType) {
        let that = this;

        /*
         * switch (viType) {
         * case "star":
         */
        let viHeader = document.createElement('div');
        viHeader.className = 'sto-option';
        viHeader.innerHTML = '</div></div><div class="sto-option-wrapper"><span class="sto-option-text">' + that.settings.option_text + '</span></div>';
        html.appendChild(viHeader);

        // break;

        /*
         * case "cross":
         *
         * break;
         * default:
         */

        //}
        return html;
    }
}

export default formatVI;
