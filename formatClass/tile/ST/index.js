import format from '../../../index.js';
import main from './main.html';

class formatST extends format {
    constructor() {
        super();
        this.type = 'ST';
        this.html;
    }
    init(settings) {
        super.init(this.settings);
        this.formatType(this.settings.type, this.html);
        this.galeryFactory(this.settings);

    }


    formatType(type, html) {
        let containerMain = html.querySelector('.sto-format');
        containerMain.innerHTML = main;
        let target = html.querySelector('.sto-logo');
        let parentNode = html.querySelector('.sto-format');
    }
    builder(galery) {
        let formatBuilder = {
            'tag': 'div',
            'class': 'sto-container',
            'galery': galery,
        };

        return formatBuilder;
    }
}
export default formatST;
