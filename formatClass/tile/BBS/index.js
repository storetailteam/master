import format from '../../../index.js';
import tile from '../abk/prod.html';
class formatBBS extends format {
    constructor() {
        super();
        this.type = 'BBS';
        this.html;
    }
    init(settings) {
        super.init(settings);
        this.galeryFactory(settings);
        this.formatBuilder = this.builder(this.galery);
        this.formatType(this.settings.type, this.html);
        this.imageAssign();
        this.colorAssign();

        /*
         *  this.trackerDesktop(this.html);
         * this.redirect("sto-format");
         */
    }
    formatType(type, html) {
        switch (type) {
            case 'abk':
                if (this.settings.frame === 'tile') {
                    let target = html.querySelector('.sto-format');
                    let parentNode = html.querySelector('.sto-container');
                    let parser = new DOMParser();
                    let htmlTreatment = parser.parseFromString(tile, 'text/html');
                    let tileExport = htmlTreatment.querySelector('.sto-product-container');

                    target.innerHTML = tile;
                    this.html = html;
                    let that = this;
                    setTimeout(function() {
                        that.optionsSetupDebug(html);
                    }, 1000);
                }
                break;
            case 'first':

                break;
            case 'cross':

                // let htmlType = document.createElement("div");
                htmlType.className = 'sto-product-container';
                html.append(htmlType);
                return htmlType;
                break;
            default:
        }
    }
    builder(galery) {
        let formatBuilder = {
            'tag': 'div',
            'class': 'sto-container',
            'galery': galery
        };
        return formatBuilder;
    }
    backupCheck(crawl, container, parentw, tilew) {

        let productsPremium = this.settings.products,
            productsBackup = this.settings.products_backup,
            premiumCounter = 0,
            backupCounter = 0,
            finalProds = [],
            capacity = Math.floor(parentw / tilew);

        productsPremium.forEach(function(id) {
            if (crawl[id]) premiumCounter++;
        });

        if (premiumCounter < (capacity - 1)) {
            productsBackup.forEach(function(id) {
                if (crawl[id]) backupCounter++;
            });
            if (backupCounter + premiumCounter >= capacity) {
                finalProds = productsPremium.concat(productsBackup);
                container.className += ' scbackup';
            }
        } else {
            finalProds = productsPremium;
        }
        return [finalProds, container];
    }
    modalBox(container, windowSize, prodImgClass, prodTitleClass, prodPriceClass) {

        // data-viewport
        let windowWidth = window.innerWidth;
        if (windowWidth > windowSize) {
            container.setAttribute('data-viewport', 'desktop');
        } else {
            container.setAttribute('data-viewport', 'mobile');
        }
        window.addEventListener('resize', function() {
            windowWidth = window.innerWidth;
            if (windowWidth > windowSize) {
                container.setAttribute('data-viewport', 'desktop');
            } else {
                container.setAttribute('data-viewport', 'mobile');
            }
        });

        let settings = this.settings;
        let that = this;
        let divModalBox = document.createElement('div');
        let divModalBoxLayout = document.createElement('div');
        divModalBox.className = 'sto-modalbox-container';
        divModalBoxLayout.className = 'sto-modalbox-layout';
        container.append(divModalBox);
        container.append(divModalBoxLayout);
        divModalBox.innerHTML += '<div class=\'sto-modalbox-header\'><span> ' + settings['modalbox_header'] + '</span></div><div class=\'sto-modalbox-close\'></div><div class=\'sto-modalbox-products\'></div><div class=\'sto-modalbox-global-abk\'><span>' + settings['modalBox_abk'] + '</div>';
        let products = container.querySelectorAll('.sto-product-container>.sto-item');

        for (let i of products) {
            container.querySelector('.sto-modalbox-products').append(i);
        }

        // vignette
        let vignetteheader = document.createElement('div');
        vignetteheader.className += 'sto-vignette-header';
        container.querySelector('.sto-product-container').append(vignetteheader);
        if (settings.header != '' && settings.header != undefined) {
            vignetteheader.innerHTML = '<span>' + settings.header + '</span>';
        }

        let vignetteMain = document.createElement('div');
        vignetteMain.className += 'sto-vignette-main';
        container.querySelector('.sto-product-container').append(vignetteMain);
        vignetteMain.innerHTML += '<div class=\'sto-vignette-image\'></div><div class=\'sto-vignette-wording\'></div><div class=\'sto-vignette-title\'></div>';

        // image vignette
        if (settings['img_sto-vignette-image'] != '' && settings['img_sto-vignette-image'] != undefined) {
            that.imageAssign();
        } else {
            for (let u of products) {
                let image = u.querySelector(prodImgClass).cloneNode();
                container.querySelector('.sto-vignette-image').append(image);
            }
        }

        // wording
        if (settings.wording != '' && settings.wording != undefined) {
            container.querySelector('.sto-vignette-wording').innerHTML = '<span>' + settings.wording + '</span>';
        }

        // title
        if (settings.title != '' && settings.title != undefined) {
            container.querySelector('.sto-vignette-title').innerHTML = '<span>' + settings.title + '</span>';
        } else {
            for (let v of products) {
                let title = v.querySelector(prodTitleClass).innerText;
                if (v === products[0]) {
                    container.querySelector('.sto-vignette-title').innerText += title;
                } else {
                    container.querySelector('.sto-vignette-title').innerText += ' + ' + title;
                }
            }
        }

        // price
        let vignettePrice = document.createElement('div');
        vignettePrice.className += 'sto-vignette-price';
        container.querySelector('.sto-product-container').append(vignettePrice);
        let bundlePrice = 0;
        for (let w of products) {
            let price = w.querySelector(prodPriceClass).innerText;
            price = Number(price.replace(/[^0-9.-]+/g,''));
            bundlePrice = bundlePrice + price;
        }
        container.querySelector('.sto-vignette-price').innerHTML = '<span>' + bundlePrice + '</span>';

        // abk
        let vignetteAbk = document.createElement('div');
        vignetteAbk.className += 'sto-vignette-abk';
        container.querySelector('.sto-product-container').append(vignetteAbk);

        // modalBox
        vignetteMain.addEventListener('click', function() {
            divModalBox.setAttribute('data-state', 'on');
            divModalBoxLayout.setAttribute('data-state', 'on');
            divModalBox.style.display = 'block';
            divModalBoxLayout.style.display = 'block';
            that.trackFormat(
                'openModal', {
                }
            );
        });
        divModalBox.querySelector('.sto-modalbox-close').addEventListener('click', function() {
            divModalBox.setAttribute('data-state', 'off');
            divModalBoxLayout.setAttribute('data-state', 'off');
            setTimeout(function() {
                divModalBox.style.display = 'none';
                divModalBoxLayout.style.display = 'none';
            }, 300);
            that.trackFormat(
                'closeModal', {
                }
            );
        });
        divModalBoxLayout.addEventListener('click', function() {
            divModalBox.setAttribute('data-state', 'off');
            divModalBoxLayout.setAttribute('data-state', 'off');
            setTimeout(function() {
                divModalBox.style.display = 'none';
                divModalBoxLayout.style.display = 'none';
            }, 300);
            that.trackFormat(
                'closeModal', {
                }
            );
        });

        // multiple same product
        let prodArray = [];
        let prodCount = {};
        for (let z of settings.productsbtns) {
            prodArray.push(z[3][0]);
        }
        prodArray.sort(function(a, b) {return a - b;});
        for (let a of prodArray) {
            if (prodCount[a]) {
                let prevCount = prodCount[a];
                prevCount++;
                prodCount[a] = prevCount;
            } else {
                prodCount[a] = 1;
            }
        }
        for (let b in prodCount) {
            if (prodCount[b] > 1) {
                container.querySelector('.sto-item[data-id="' + b + '"]').innerHTML += '<div class=\'sto-prod-count\'>x' + prodCount[b] + '</div>';
            }
        }

    }
}

export default formatBBS;
