import format from '../../../index.js';
import tile from './main.html';

class formatPMP extends format {
    constructor() {
        super();
        this.type = 'PMP';
        this.html;
    }
    init(settings) {
        super.init(settings);
        this.galeryFactory(settings);
        this.formatBuilder = this.builder(this.galery);
        this.formatType(this.settings.type, this.html);
    }
    formatType(type, html) {
        switch (type) {
            case 'abk':
                if (this.settings.frame === 'tile') {
                    let target = html.querySelector('.sto-format');
                    let parentNode = html.querySelector('.sto-' + __TYPE__ + '-container');
                    let parser = new DOMParser();
                    let htmlTreatment = parser.parseFromString(tile, 'text/html');
                    let tileExport = htmlTreatment.querySelector('.sto-product-container');
                    target.innerHTML = tile;
                    this.html = html;
                }
                break;
            case 'first':

                break;
            case 'cross':

                // let htmlType = document.createElement("div");
                htmlType.className = 'sto-product-container';
                html.append(htmlType);
                return htmlType;
                break;
            default:
        }
    }
    builder(galery) {
        let formatBuilder = {
            'tag': 'div',
            'class': 'sto-' + __TYPE__ + '-container',
            'galery': galery
        };
        return formatBuilder;
    }
    btfButtons(prods, container) {
        let that = this,
            buttonWrapper = document.createElement('div'),
            productExiste = 0,
            justHover,
            btns = {};
        if (that.settings['img_sto-button-normal'] && that.settings['img_sto-button-normal'] != '') {
            container.querySelector('.sto-buttons-wrapper').setAttribute('button-normal', 'img');
        } else {
            container.querySelector('.sto-buttons-wrapper').setAttribute('button-normal', 'color');
        }
        if (that.settings['img_sto-button-selected'] && that.settings['img_sto-button-selected'] != '') {
            container.querySelector('.sto-buttons-wrapper').setAttribute('button-selected', 'img');
        } else {
            container.querySelector('.sto-buttons-wrapper').setAttribute('button-selected', 'color');
        }

        for (let index in prods) {
            let realProducts;
            productExiste = 0;
            if (index < container.querySelectorAll('.sto-product-container>*').length) {
                realProducts = container.querySelectorAll('.sto-product-container>*')[index].getAttribute('data-id');
            } else {
                realProducts = '';
            }
            for (let sub in prods[index][3]) {

                if (container.querySelectorAll('.sto-product-container>*[data-id=\'' + prods[index][3][sub] + '\']').length > 0 && productExiste < 1) {
                    let button = document.createElement('div');
                    button.className = 'sto-button sto-button-normal';
                    button.setAttribute('data-index', index);
                    button.setAttribute('data-id', prods[index][3][sub]);
                    button.setAttribute('data-title', prods[index][1]);
                    button.style.width = that.settings.button_width && that.settings.button_width != '' ? this.settings.button_width + 'px' : 'calc(100% - 10px)';
                    button.style.fontSize = that.settings.button_fontsize && that.settings.button_fontsize != '' ? that.settings.button_fontsize + 'px' : '12px';
                    button.innerHTML = '<span>' + prods[index][1] + '</span>';
                    btns.cid = that.settings.cid;
                    btns.buttons = button;

                    justHover = false;
                    button.addEventListener('mouseenter', function () {
                        if (this.className.indexOf('sto-button-selected') == -1) {
                            this.className = 'sto-button sto-button-selected';
                            if (__TYPE__ !== 'api') {
                                that.imageAssign(container);
                                that.colorAssign(container);
                            }
                            justHover = true;
                        }
                    });
                    button.addEventListener('mouseleave', function () {
                        if (justHover == true && this.className.indexOf('sto-button-selected') > -1) {
                            this.className = 'sto-button sto-button-normal';
                            if (__TYPE__ !== 'api') {
                                that.imageAssign(container);
                                that.colorAssign(container);
                            }
                            justHover = false;
                        }
                    });
                    button.addEventListener('click', function () {

                        for (let k = 0; k < container.querySelectorAll('.sto-product-container>*').length; k++) {
                            container.querySelectorAll('.sto-product-container>*')[k].style.display = 'none';
                        }
                        container.querySelector('.sto-product-container>*[data-id="' + prods[index][3][sub] + '"]').style.display = 'inline-block';
                        for (let l = 0; l < container.querySelectorAll('div.sto-button').length; l++) {
                            container.querySelectorAll('div.sto-button')[l].className = 'sto-button sto-button-normal';
                        }
                        this.className = 'sto-button sto-button-selected';
                        that.trackFormat(
                            'trackBrowse', {
                                id: this.getAttribute('data-id'),
                                title: this.getAttribute('data-title')
                            }
                        );
                        if (__TYPE__ !== 'api') {
                            that.imageAssign(container);
                            that.colorAssign(container);
                        }
                        justHover = false;
                    });

                    if (container.className.indexOf(btns.cid) > -1) {
                        container.querySelector('.sto-buttons-wrapper').appendChild(btns.buttons);
                    }

                    productExiste++;
                }
            }
            if (index >= 4) {
                break;
            }

        }
        container.querySelectorAll('div.sto-button')[0].className = 'sto-button sto-button-selected';
        that.optionsSetup(container);

        that.multiBackground(container);

        return container;
    }

    multiBackground(container) { //Function:if multibackground is slected, set it up
        let that = this;
        if (that.settings.multibackground && that.settings.multibackground === true) { //is multiBackground checked
            //set first image
            let allButtons = container.querySelectorAll('div.sto-button');
            let bIndex = parseInt(allButtons[0].getAttribute('data-index'));
            container.querySelector('.sto-left-side').className = 'sto-left-side sto-image-container' + (bIndex + 1);
            that.imageAssign();

            //bind the buttons to change classes and trigger image func
            for (let v = 0; v < allButtons.length; v++) {
                allButtons[v].addEventListener('click', function () {
                    bIndex = parseInt(this.getAttribute('data-index'));
                    container.querySelector('.sto-left-side').className = 'sto-left-side sto-image-container' + (bIndex + 1);
                    that.imageAssign();
                });
            }
        }
        return container;
    }

}

export default formatPMP;
