import format from '../../../index.js';
import tile from '../abk/prod.html';

class formatPS extends format {
    constructor() {
        super();
        this.type = 'PS';
        this.html;
    }
    init(settings) {
        super.init(settings);
        this.galeryFactory(settings);
        this.formatBuilder = this.builder(this.galery);
        this.formatType(this.settings.type, this.html);
        this.wording(this.html);
        this.colorAssign();
    }
    formatType(type, html) {
        switch (type) {
            case 'abk':
                if (this.settings.frame === 'tile') {
                    let target = html.querySelector('.sto-format');
                    let parentNode = html.querySelector('.sto-container');
                    let parser = new DOMParser();
                    let htmlTreatment = parser.parseFromString(tile, 'text/html');
                    let tileExport = htmlTreatment.querySelector('.sto-product-container');
                    target.innerHTML = tile;
                    this.html = html;
                }
                break;
            case 'first':

                break;
            case 'cross':

                // let htmlType = document.createElement("div");
                htmlType.className = 'sto-product-container';
                html.append(htmlType);
                return htmlType;
                break;
            default:
        }
    }
    builder(galery) {
        let formatBuilder = {
            'tag': 'div',
            'class': 'sto-container',
            'galery': galery
        };
        return formatBuilder;
    }


}

export default formatPS;
