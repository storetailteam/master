import htmlImport from './main.html';
import mobile from './formatClass/mobile/index.js';

class format {
    constructor() {
        this.type = '';
        this.settings;
        this.tracker;
        this.galery;
        this.formatBuilder;
        this.mandatory;
    }
    getType() {
        return this.type;
    }
    galeryFactory(settings) {        
        const galery = {};
        galery[settings.cid] = {
            'color': {
                'background': {},
                'text': {},
                'border': {}
            },
            'images': {}
        };
        for (let index in settings) {
            if (index.split('_')[0] === 'img' && index.indexOf('__') < 0) {
                if (settings[index] !== '') {
                    let classTarget = index.split('_')[1];
                    if (classTarget === 'sto-container') {
                        galery[settings.cid]['images']['sto-__TAG__-container'] = settings.path + '/' + settings[index] + '?' + Date.now();
                    } else {
                        galery[settings.cid]['images'][index.split('_')[1]] = settings.path + '/' + settings[index] + '?' + Date.now();
                    }
                } else {
                    galery[settings.cid]['images'][index.split('_')[1]] = '';
                }
            } else if (index.split('_')[0] === 'color-bg') {
                if (settings[index] !== '') {
                    let classTarget = index.split('_')[1];
                    if (classTarget === 'sto-container') {
                        galery[settings.cid]['color']['background']['sto-__TAG__-container'] = settings[index];
                    } else {
                        galery[settings.cid]['color']['background'][index.split('_')[1]] = settings[index];
                    }
                } else {
                    galery[settings.cid]['color']['background'][index.split('_')[1]] = 'transparent';
                }
            } else if (index.split('_')[0] === 'color-txt') {
                if (settings[index] !== '') {
                    let classTarget = index.split('_')[1];
                    if (classTarget === 'sto-container') {
                        galery[settings.cid]['color']['text']['sto-__TAG__-container'] = settings[index];
                    } else {
                        galery[settings.cid]['color']['text'][index.split('_')[1]] = settings[index];
                    }
                } else {
                    galery[settings.cid]['color']['text'][index.split('_')[1]] = 'transparent';
                }
            } else if (index.split('_')[0] === 'color-border') {
                if (settings[index] !== '') {
                    let classTarget = index.split('_')[1];
                    if (classTarget === 'sto-container') {
                        galery[settings.cid]['color']['border']['sto-__TAG__-container'] = settings[index];
                    } else {
                        galery[settings.cid]['color']['border'][index.split('_')[1]] = settings[index];
                    }
                } else {
                    galery[settings.cid]['color']['border'][index.split('_')[1]] = 'lightgray';
                }
            }
        }
        this.galery = galery;
        return this.galery;
    }

    addViewTracking(target, tracker, callback) {
        let int;

        let isElementInViewport = ((el) => {
            let rect = el.getBoundingClientRect();

            return (
                rect.top >= 0 &&
                rect.left >= 0 &&
                !(rect.top == rect.bottom || rect.left == rect.right) &&
                !(rect.height == 0 || rect.width == 0) &&
                rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
                rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
            );

        });

        let isElement = ((o) => {
            return (
                typeof HTMLElement === 'object' ? o instanceof HTMLElement : //DOM2
                    o && typeof o === 'object' && o !== null && o.nodeType === 1 && typeof o.nodeName === 'string'
            );
        });

        if (typeof window.jQuery === 'function' && target instanceof window.jQuery) {
            target = target[0];
        }

        int = window.setInterval(() => {
            let container = typeof target === 'string' ? document.querySelector(target) : target;
            if (isElement(container) && isElementInViewport(container)) {
                tracker.view();
                if (callback && typeof callback === 'function') {
                    callback();
                }
                window.clearInterval(int);
            }
        }, 100);
    }

    imageAssign(container) {
        const that = this;
        for (let index in this.galery[that.settings.cid]['images']) {
            if (this.html.querySelector('.sto-' + __TYPE__ + '-container[data-type=\'' + this.type + '\'] .' + index)) {
                let target;
                if (container !== undefined && container !== '') {
                    target = container.querySelectorAll('.' + index);
                } else {
                    target = this.html.querySelectorAll('.sto-' + __TYPE__ + '-container[data-type=\'' + this.type + '\'] .' + index);
                }
                for (let i = 0; i < target.length; i++) {
                    if (target[i] !== null && target[i] !== undefined) {
                        target[i].style.backgroundImage = 'url(' + this.galery[that.settings.cid]['images'][index] + ')';
                    }
                }
            }
        }
    }
    colorAssign(container) {
        const that = this;
        for (let index in this.galery[that.settings.cid]['color']['background']) {
            if ((that.html.querySelector('.sto-' + __TYPE__ + '-container[data-type=\'' + that.type + '\'] .' + index) || index === 'sto-' + __TYPE__ + '-container') && that.html.getAttribute('data-creaid') == that.settings.cid || container !== undefined) {
                //let target = index === 'sto-' + __TYPE__ + '-container' ? that.html.parentNode.querySelector('.sto-' + __TYPE__ + '-container[data-type=\'' + that.type + '\'][data-creaid=\'' + that.settings.cid + '\']') : that.html.parentNode.querySelectorAll('.sto-' + __TYPE__ + '-container[data-type=\'' + that.type + '\'][data-creaid=\'' + that.settings.cid + '\'] .' + index);
                let target;
                if (container !== undefined) {
                    target = index === 'sto-' + __TYPE__ + '-container' ? container : container.querySelectorAll('.' + index);
                } else {
                    target = index === 'sto-' + __TYPE__ + '-container' ? that.html.parentNode.querySelector('.sto-' + __TYPE__ + '-container[data-type=\'' + that.type + '\'][data-creaid=\'' + that.settings.cid + '\']') : that.html.parentNode.querySelectorAll('.sto-' + __TYPE__ + '-container[data-type=\'' + that.type + '\'][data-creaid=\'' + that.settings.cid + '\'] .' + index);
                }

                if (index === 'sto-' + __TYPE__ + '-container') {
                    target.style.backgroundColor = that.galery[that.settings.cid]['color']['background'][index];
                } else {
                    for (let i = 0; i < target.length; i++) {
                        if (target[i] !== null && target[i] !== undefined) {
                            target[i].style.backgroundColor = that.galery[that.settings.cid]['color']['background'][index];
                        }
                    }
                }
            }
            if (this.html.className.indexOf('sto-' + __TYPE__ + '-container') > -1 && this.html.getAttribute('data-type') == this.type) {
                let target = this.html;
                target.style.backgroundColor = this.galery[that.settings.cid]['color']['background']['sto-' + __TYPE__ + '-container'];
            }
        }
        for (let index in this.galery[that.settings.cid]['color']['text']) {
            if (this.html.querySelector('.sto-' + __TYPE__ + '-container[data-type=\'' + that.type + '\'] .' + index) || index === 'sto-' + __TYPE__ + '-container' || container !== undefined) {
                //let target = index === 'sto-' + __TYPE__ + '-container' ? that.html : that.html.querySelectorAll('.sto-' + __TYPE__ + '-container[data-type=\'' + that.type + '\'] .' + index);

                let target;
                if (container !== undefined) {
                    target = index === 'sto-' + __TYPE__ + '-container' ? container : container.querySelectorAll('.' + index);
                } else {
                    target = index === 'sto-' + __TYPE__ + '-container' ? that.html : that.html.querySelectorAll('.sto-' + __TYPE__ + '-container[data-type=\'' + that.type + '\'] .' + index);
                }

                if (index === 'sto-' + __TYPE__ + '-container') {
                    target.style.color = that.galery[that.settings.cid]['color']['text'][index];
                } else {
                    for (let i = 0; i < target.length; i++) {
                        if (target[i] !== null && target[i] !== undefined) {
                            target[i].style.color = that.galery[that.settings.cid]['color']['text'][index];
                        }
                    }
                }
            }
        }
        for (let index in that.galery[that.settings.cid]['color']['border']) {
            if (this.html.querySelector('.sto-' + __TYPE__ + '-container[data-type=\'' + this.type + '\'] .' + index) || index === 'sto-' + __TYPE__ + '-container' || container !== undefined) {
                //let target = index === 'sto-' + __TYPE__ + '-container' ? this.html : this.html.querySelectorAll('.sto-' + __TYPE__ + '-container[data-type=\'' + this.type + '\'] .' + index);

                let target;
                if (container !== undefined) {
                    target = index === 'sto-' + __TYPE__ + '-container' ? container : container.querySelectorAll('.' + index);
                } else {
                    target = index === 'sto-' + __TYPE__ + '-container' ? this.html : this.html.querySelectorAll('.sto-' + __TYPE__ + '-container[data-type=\'' + this.type + '\'] .' + index);
                }

                if (index === 'sto-' + __TYPE__ + '-container') {
                    target.style.borderColor = that.galery[that.settings.cid]['color']['border'][index];
                } else {
                    for (let i = 0; i < target.length; i++) {
                        if (target[i] !== null && target[i] !== undefined) {
                            target[i].style.borderColor = that.galery[that.settings.cid]['color']['border'][index];
                        }
                    }
                }
            }
        }
    }
    getSettings(settings) {
        this.settings = settings;
        return this.settings;
    }
    newMandatoryProduct(settings, crawl) {
        this.mandatory = false;
        for (let i of settings.productsbtns) {
            const mandatoryQty = 0;
            if (i[2] === true) {
                for (let j of i[3]) {
                    if (crawl[j]) mandatoryQty++;
                }
                if (mandatoryQty < 1) this.mandatory = true;
            }
        }
    }
    backupProducts(settings, crawl) {
        this.newMandatoryProduct(settings, crawl);
        const ctxt_prodlist = 3;
        const products = [];
        if (settings.productsbtns && settings.productsbtns.length > 0 && settings.productsbtns[0].length === 4 && Array.isArray(settings.productsbtns[0][3])) {

            const productsLists = settings.productsbtns;
            productsLists.sort();
            productsLists.forEach(function (e) {
                if (Array.isArray(e[ctxt_prodlist])) {
                    e[ctxt_prodlist].forEach(function (p) {
                        products.push(p.trim());
                    });
                } else {
                    products.push(e)
                }
            })
            settings.products = products;
        } else {
            settings.products = settings.productsbtns
        }
        return settings
    }
    getProductsBtn(settings) {
        const ctxt_prodlist = 3;
        const products = [];
        if (settings.productsbtns && settings.productsbtns.length > 0 && settings.productsbtns[0].length == 4 && Array.isArray(settings.productsbtns[0][3])) {
            const productsLists = settings.productsbtns;
            productsLists.sort();
            productsLists.forEach(function (e) {
                if (Array.isArray(e[ctxt_prodlist])) {
                    e[ctxt_prodlist].forEach(function (p) {
                        products.push(p);
                    });
                } else {
                    products.push(e);
                }
            });
            settings.products = products;
        } else {
            settings.products = settings.productsbtns;
        }
        return settings;
    }
    mandatoryProduct(settings, crawl) {
        let mandatory = false;
        let mandatoryQty = 0;
        for (let index in settings.productsbtns) {
            if (settings.productsbtns[index][2] === true) {
                for (let i = 0; i < settings.productsbtns[index][3].length; i++) {
                    if (crawl[settings.productsbtns[index][3][i]]) {
                        mandatoryQty++;
                    }
                }
            }
            if (mandatoryQty < 1) {
                return false;
            } else {
                return true;
            }
        }
    }
    getTrackers(tracker) {
        this.tracker = tracker;
        return this.tracker;
    }
    trackFormat(eventTrack, param) {
        switch (eventTrack) {
            case 'imp':
                this.tracker.display(param);
                break;
            case 'view':
                this.tracker.view({
                    'tl': 'view'
                });
                break;
            case 'redirection':
                this.tracker.click();
                break;
            case 'file':
                this.tracker.openPDF();
                break;
            case 'openvideo':
                this.tracker.playVideo();
                break;
            case 'closevideo':
                this.tracker.closeVideo();
                break;
            case 'trackBrowse':
                this.tracker.browse({
                    'tl': param.id,
                    'pl': param.title,
                    'pi': param.id
                });
                break;
            case 'closeFormat':
                this.tracker.close();
                break;
            default:
        }
    }

    // à effacer et à vérifier solutions qui l'utilisent
    productsCF(c) {
        const products = this.settings.productsbtns,
            productsList = [];
        if (Array.isArray(products[0])) {
            if (products[0].length < 3) {
                products.forEach(function (v, i) {
                    productsList[i] = v[1];
                });
            } else {
                let mandatoryQty = 0;
                for (let index in products) {
                    if (products[index][2] === true) {
                        for (let i = 0; i < products[index][3].length; i++) {
                            let mandatoryQty = 0;
                            if (c[products[index][3][i]]) {
                                mandatoryQty++;
                            }
                        }
                        if (mandatoryQty < 1) {
                            productsList = [];
                        }
                    }
                    productsList = productsList.concat(products[index][3]);
                }
            }
        } else {
            productsList = products;
        }
        return productsList;
    }

    optionsType(optionContainer, container, obj) {

        const that = this;
        let count = 1;
        for (let index in obj) {
            let counter = count === 1 ? '' : count;
            switch (obj[index]['type']) {
                case 'redirection':
                    if (this.type === 'SC') {
                        container.querySelector('.sto-image-container').style.pointerEvents = 'none';
                    }
                    if (obj[index]['id'] === 'option') {
                        obj['option']['element'].addEventListener('click', function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                            that.trackFormat('redirection', false);
                            window.open(obj['option']['redir'], obj['option']['target']);
                        });
                        if (container.hasAttribute('data-viewport') && container.querySelector('.sto-logo')) {
                            if (container.getAttribute('data-viewport') == 'mobile' && this.type !== 'SC') {
                                container.querySelector('.sto-logo').addEventListener('click', function () {
                                    that.trackFormat('redirection', false);
                                    window.open(obj["option"]["redir"], obj["option"]["target"]);
                                });
                            }
                        }
                        if (container.querySelector('.sto-logo-mobile')) {
                            container.querySelector('.sto-logo-mobile').addEventListener('click', function () {
                                that.trackFormat('redirection', false);
                                window.open(obj["option"]["redir"], obj["option"]["target"]);
                            });
                        }
                    } else if (obj[index]['id'] === 'option2') {
                        obj['option2']['element'].addEventListener('click', function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                            that.trackFormat('redirection', false);
                            window.open(obj['option2']['redir'], obj['option2']['target']);
                        });
                        if (container.hasAttribute('data-viewport')) {
                            if (container.getAttribute('data-viewport') == 'mobile') {
                                container.querySelector('.sto-logo').addEventListener('click', function () {
                                    that.trackFormat('redirection', false);
                                    window.open(obj["option2"]["redir"], obj["option2"]["target"]);
                                });
                            }
                        }
                        if (container.querySelector('.sto-logo-mobile')) {
                            container.querySelector('.sto-logo-mobile').addEventListener('click', function () {
                                that.trackFormat('redirection', false);
                                window.open(obj["option2"]["redir"], obj["option2"]["target"]);
                            });
                        }
                    }
                    break;
                case 'legal':

                    break;
                case 'file':
                    obj['option']['element'].addEventListener('click', function (e) {
                        e.preventDefault();
                        e.stopPropagation();
                        that.trackFormat('file', false);
                        window.open(that.settings.path + "/" + that.settings.file, obj["option"]["target"]);
                    });
                    if (container.hasAttribute('data-viewport')) {
                        if (container.getAttribute('data-viewport') == 'mobile') {
                            container.querySelector('.sto-logo').addEventListener('click', function () {
                                that.trackFormat('redirection', false);
                                window.open(that.settings.path + "/" + that.settings.file, obj["option"]["target"]);
                            });
                        }
                    }
                    if (container.querySelector('.sto-logo-mobile')) {
                        container.querySelector('.sto-logo-mobile').addEventListener('click', function () {
                            that.trackFormat('redirection', false);
                            window.open(that.settings.path + "/" + that.settings.file, obj["option"]["target"]);
                        });
                    }
                    break;
                case 'video':
                    let videoInteractions = function () {
                        if (!document.querySelector('body .sto-video')) {
                            that.trackFormat('openvideo', false);
                            let body = document.querySelector('body'),
                                video = document.createElement('div'),
                                videoBg = document.createElement('div');
                            video.className = 'sto-video';
                            video.innerHTML = '<div class="sto-close-video"></div><div class="sto-video-wrapper">' + that.settings.video_iframe + '</div>';
                            video.innerHTML = '<div class="sto-close-video" style="background-image:url(https://rscdn.storetail.net/close.png)"></div><div class="sto-video-wrapper">' + that.settings.video_iframe + '</div>';
                            videoBg.className = 'sto-video-background';
                            body.appendChild(videoBg);
                            body.appendChild(video);
                            videoBg.addEventListener('click', function () {
                                that.trackFormat('closevideo', false);
                                body.removeChild(video);
                                body.removeChild(this);
                            });
                            video.querySelector('.sto-close-video').addEventListener('click', function () {
                                that.trackFormat('closevideo', false);
                                body.removeChild(video);
                                body.removeChild(videoBg);
                            });
                        }
                    };

                    obj['option']['element'].addEventListener('click', function (e) {
                        e.preventDefault();
                        e.stopPropagation();
                        videoInteractions();
                    });

                    if (container.querySelector('.sto-logo-mobile')) {
                        container.querySelector('.sto-logo-mobile').addEventListener('click', function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                            videoInteractions();
                        });
                    }
                    break;
            }
            count++;
        }
    }
    optionsSetup(container, url) {
        const that = this;
        if (this.settings.multibrand === true) {
            container.setAttribute('data-option', 'multibrand');
            return container;
        }
        url = url !== undefined ? url : this.settings.redirection_url;
        this.settings.redirection_url = url;
        const obj = {};
        let count = 1,
            settings = this.settings,
            optionContainer;
        for (let index in this.settings) {
            if (index.indexOf('option') > -1 && index.indexOf('option') === 0 && index.indexOf('option_') < 0) {
                let counter = index.replace('option', '');
                if (that.settings[index] !== '' && that.settings[index] !== 'classic') {
                    obj[index] = {
                        id: index,
                        type: that.settings[index],
                        redir: that.settings['redirection_url' + counter],
                        target: that.settings['redirection_target' + counter]
                    };
                }
                count++;
            }
        }
        if (that.settings.option || that.settings.option2) {
            count = 1;
            for (let index in obj) {
                let counter = count === 1 ? '' : count;
                let opt = counter === '' ? 'option' : 'option2';
                optionContainer = container.querySelector('.sto-' + obj[index]['id']);
                if (that.settings.frame == 'buttons') {
                    optionContainer.innerHTML = '<div class="sto-' + obj[index]['id'] + '-text">' + settings['sto_option_text' + counter] + '</div>';
                    obj['option']['element'] = optionContainer;
                    container.querySelector('.sto-buttons').appendChild(optionContainer);
                    container.setAttribute('data-count', parseFloat(container.getAttribute('data-count')) + 1);
                } else {
                    if (obj[index]['id'] === 'option') {
                        container.setAttribute('data-option', obj['option']['type']);
                        optionContainer.setAttribute('data-option', obj['option']['type']);
                        obj['option']['element'] = optionContainer;
                        optionContainer.setAttribute('data-type', obj['option']['type']);
                        container.querySelector('.sto-' + obj['option']['id']).innerHTML = '<div class="sto-option-wrapper"><span class="sto-option-text">' + that.settings['sto_option_text'] + '</span></div>';
                        if (that.settings.button_fontsize && that.settings.button_fontsize != '') {
                            container.querySelector('.sto-option-text').style.fontSize = that.settings.button_fontsize;
                        }
                    }
                    if (obj[index]['id'] === 'option2' && obj[index]['type'] !== '') {
                        container.setAttribute('data-option2', obj['option2']['type']);
                        optionContainer.setAttribute('data-option2', obj['option2']['type']);
                        obj['option2']['element'] = optionContainer;
                        container.querySelector('.sto-' + obj['option2']['id']).innerHTML = '<div class="sto-option-wrapper2"><span class="sto-option-text2">' + that.settings['sto_option_text2'] + '</span></div>';
                        optionContainer.setAttribute('data-option', 'option2');
                        optionContainer.setAttribute('data-type', obj['option2']['type']);
                        optionContainer.style.display = 'block';
                        container.querySelector('.sto-option').setAttribute('double-cta', '');
                        if (that.settings.button_fontsize && that.settings.button_fontsize != '') {
                            container.querySelector('.sto-option-text2').style.fontSize = that.settings.button_fontsize;
                        }
                    }
                    if (obj[index]['id'] === 'option' && obj[index]['type'] === '') {
                        if (this.settings.vignette_type === 'redirection' || this.settings.vignette_type === 'companion') {
                            optionContainer.addEventListener('click', function () {
                                that.trackFormat('redirection', false);
                                window.open(that.settings.redirection_url, that.settings.redirection_target);
                            });
                        }
                    }
                }
                count++;
            }
        } else {
            if (this.settings.vignette_type === 'fixed' || this.settings.vignette_type === 'companion') {
                container.querySelector('.sto-option').innerHTML = '<div class="sto-option-wrapper"><span class="sto-option-text">' + that.settings['sto_option_text'] + '</span></div>';
                container.addEventListener('click', function () {
                    that.trackFormat('redirection', false);
                    window.open(url, that.settings.redirection_target);
                });
            }
        }
        that.optionsType(optionContainer, container, obj);
        if (__TYPE__ !== "api") {
            that.imageAssign()
            that.colorAssign();
            that.imageAssign();
        }
        return container;
    }
    redirectionSimple(obj) {
        const that = this;
        obj['container'].querySelector(obj['element']).addEventListener('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            that.trackFormat('redirection', false);
            window.open(obj['redir'], obj['target']);
        });
    }
    mobileHomotetie(container, hEl) {
        const that = this;
        hEl = container.querySelector('.' + hEl) ? hEl.replace('.', '') : 'sto-left-side';

        if ((container.getAttribute('data-viewport') === 'mobile' && !container.hasAttribute('data-website')) || (container.getAttribute('data-viewport') === 'mobile' && container.getAttribute('data-website') === 'desktop') || (container.getAttribute('data-viewport') === 'mobile' && container.getAttribute('data-website') === 'mobile') || (!container.hasAttribute('data-viewport') && container.getAttribute('data-website') === 'mobile')) {

            let containerWidth = container.offsetWidth,
                headerHeight = Math.round((containerWidth * 300) / 1242);
            if (container.className.indexOf(hEl) > -1) { } else {
                container.querySelector('.' + hEl).style.height = headerHeight + 'px';
            }
        } else {
            if (container.className.indexOf(hEl) > -1) {
                container.style.height = 'inherit';
            } else {
                if (container.className.indexOf(hEl) > -1) {
                    container.style.height = 'inherit';
                } else {
                    container.querySelector('.' + hEl).style.height = 'inherit';
                }
            }
        }
        that.colorAssign();
        that.imageAssign();

    }
    setViewPort(container, mobSize, homotetieEl, tile, grid) {
        const that = this;
        let containerWidth,
            data_size,
            tilesPerRow;

        containerWidth = that.settings.frame !== "tile" ? container.offsetWidth : container.parentNode.offsetWidth,
            data_size = 99;

        tilesPerRow = Math.round(grid.offsetWidth / tile.offsetWidth);

        if (containerWidth < 768 || containerWidth <= mobSize) {
            data_size = 1;

        } else if (containerWidth <= 961) {
            data_size = 2;
        } else if (containerWidth < 1152 && containerWidth >= 962) {
            data_size = 3;
        } else if (containerWidth <= 1152) {
            data_size = 4;
        } else if (containerWidth > 1152) {
            data_size = 5;
        }
        container.setAttribute("data-size", data_size);

        mobSize = mobSize !== undefined ? mobSize : 575;


        if (window.matchMedia('(max-width: ' + mobSize + 'px)').matches) {
            container.setAttribute('data-viewport', 'mobile');
        } else {
            container.setAttribute('data-viewport', 'desktop');
        }

        if (that.settings.formatClass == 'banner') {
            if (that.settings.frame == "buttons") {
                container.setAttribute('data-frame', 'btn');
            }
            if (container.querySelector('.sto-design-right')) {
                if (container.querySelector('.sto-design-right').offsetWidth > 210 && container.getAttribute('data-frame') !== "btn") {
                    container.querySelector('.sto-design-right').className = 'sto-design-right sto-design-right-288';
                } else if (container.getAttribute('data-size') === "3" && container.getAttribute('data-option') === "multibrand") {
                    container.querySelector('.sto-design-right').className = 'sto-design-right sto-design-right-138';
                } else {
                    container.querySelector('.sto-design-right').className = 'sto-design-right sto-design-right-192';
                }
            }
            if (container.querySelector('.sto-design-left')) {
                if (container.getAttribute('data-size') === "5" && container.getAttribute('data-option') === "multibrand") {
                    container.querySelector('.sto-design-left').className = 'sto-design-left sto-design-left-192';
                } else if (container.getAttribute('data-size') === "2" && container.getAttribute('data-count') === "1" && container.getAttribute('data-frame') === "btn") {
                    container.querySelector('.sto-design-left').className = 'sto-design-left sto-design-left-big';
                }
                else {
                    container.querySelector('.sto-design-left').className = 'sto-design-left';
                }
            }

            if (__TYPE__ !== 'api') {
                that.imageAssign();
                that.colorAssign();
            }
        }
        return container;
    }
    breakPoints(container, mobSize, homotetieEl, tile, grid,) {
        const that = this;
        let resultContainer;

        resultContainer = that.setViewPort(container, mobSize, homotetieEl, tile, grid);

        window.addEventListener('resize', function () {
            resultContainer = that.setViewPort(container, mobSize, homotetieEl, tile, grid);
        });

        return resultContainer;
    }

    setDataSize(container, mobSize, homotetieEl, tile, grid) {
        let that = this,
            containerWidth,
            data_size = 99,
            tilesPerRow;
        mobSize = mobSize !== undefined ? mobSize : 575;
        containerWidth = that.settings.frame !== "tile" ? container.offsetWidth : container.parentNode.offsetWidth,
            tilesPerRow = Math.round(grid.offsetWidth / tile.offsetWidth);

        if (containerWidth < 768) {
            data_size = 1;

        } else if (containerWidth < 768) {
            data_size = 2;
        } else if (containerWidth < 864) {
            data_size = 3;
        } else if (containerWidth <= 1152) {
            data_size = 4;
        } else if (containerWidth > 1152) {
            data_size = 5;
        }


        container.setAttribute("data-size", data_size);

        return container;
    }

    setDataSize(container, mobSize, homotetieEl, tile, grid) {
        let that = this,
            containerWidth,
            data_size = 99,
            tilesPerRow;
        mobSize = mobSize !== undefined ? mobSize : 575;
        containerWidth = that.settings.frame !== "tile" ? container.offsetWidth : container.parentNode.offsetWidth,
            tilesPerRow = Math.round(grid.offsetWidth / tile.offsetWidth);

        if (containerWidth < 768) {
            data_size = 1;

        } else if (containerWidth < 768) {
            data_size = 2;
        } else if (containerWidth < 864) {
            data_size = 3;
        } else if (containerWidth <= 1152) {
            data_size = 4;
        } else if (containerWidth > 1152) {
            data_size = 5;
        }


        container.setAttribute("data-size", data_size);

        return container;
    }

    miniProducsSetup(container, targetImg) {
        let updateImgs = function (c) {

            let rightId = c.querySelector('.sto-arrow-right').getAttribute('data-id'),
                leftId = c.querySelector('.sto-arrow-left').getAttribute('data-id');

            let rightImg = c.querySelector('.sto-product-container>*[data-id="' + rightId + '"] ' + targetImg).hasAttribute('src') ? c.querySelector('.sto-product-container>*[data-id="' + rightId + '"] ' + targetImg).getAttribute('src') : c.querySelector('.sto-product-container>*[data-id="' + rightId + '"] ' + targetImg).getAttribute('srcset');
            let leftImg = c.querySelector('.sto-product-container>*[data-id="' + leftId + '"] ' + targetImg).hasAttribute('src') ? c.querySelector('.sto-product-container>*[data-id="' + leftId + '"] ' + targetImg).getAttribute('src') : c.querySelector('.sto-product-container>*[data-id="' + leftId + '"] ' + targetImg).getAttribute('srcset');
            if (c.querySelector('.sto-product-container>*[data-id="' + rightId + '"] ' + targetImg).hasAttribute('src')) {
                c.querySelector('.sto-arrow-right.sto-mini-product img').setAttribute('src', rightImg);
            } else {
                c.querySelector('.sto-arrow-right.sto-mini-product img').setAttribute('srcset', rightImg);
            }
            if (c.querySelector('.sto-product-container>*[data-id="' + leftId + '"] ' + targetImg).hasAttribute('src')) {
                c.querySelector('.sto-arrow-left.sto-mini-product img').setAttribute('src', leftImg);
            } else {
                c.querySelector('.sto-arrow-left.sto-mini-product img').setAttribute('srcset', leftImg);
            }
        };

        for (let i = 0; i < 2; i++) {
            container.querySelectorAll('.sto-arrows')[i].addEventListener('click', function () {
                setTimeout(function () {
                    updateImgs(container);
                }, 50);
            });
            container.querySelectorAll('.sto-mini-product')[i].addEventListener('click', function () {
                setTimeout(function () {
                    updateImgs(container);
                }, 50);
            });
        }
        updateImgs(container);
        setTimeout(function () {
            updateImgs(container);
        }, 200);
        return container;
    }
    buttonsSetup(container, crawl) {
        const that = this;
        let i, j, k, l, m,
            justHover,
            productsInSettings = that.settings.productsbtns,
            buttonWrapper = container.querySelector('.sto-buttons-wrapper');
        if (that.settings['img_sto-button-normal'] && that.settings['img_sto-button-normal'] != '') {
            buttonWrapper.setAttribute('button-normal', 'img');
        } else {
            buttonWrapper.setAttribute('button-normal', 'color');
        }
        if (that.settings['img_sto-button-selected'] && that.settings['img_sto-button-selected'] != '') {
            buttonWrapper.setAttribute('button-selected', 'img');
        } else {
            buttonWrapper.setAttribute('button-selected', 'color');
        }

        for (let indexProduct = 0; indexProduct < productsInSettings.length; indexProduct++) {
            let pdts = productsInSettings[indexProduct][3];
            for (let indexProdsIds = 0; indexProdsIds < pdts.length; indexProdsIds++) {
                if (crawl[productsInSettings[indexProduct][3][indexProdsIds]]) {
                    let button = document.createElement('button');

                    button.className = 'sto-button sto-button-normal';
                    button.setAttribute('data-index', indexProduct);
                    button.setAttribute('data-id', productsInSettings[indexProduct][3][parseFloat(indexProdsIds)]);
                    button.setAttribute('data-title', productsInSettings[indexProduct][1]);
                    button.style.width = this.settings.button_width && this.settings.button_width != '' ? this.settings.button_width + 'px' : '150px';
                    button.style.fontSize = this.settings.button_fontsize && this.settings.button_fontsize != '' ? this.settings.button_fontsize + 'px' : '13px';
                    button.innerText = productsInSettings[indexProduct][1];

                    justHover = false;
                    button.addEventListener('mouseenter', function () {
                        if (this.className.indexOf('sto-button-selected') == -1) {
                            this.className = 'sto-button sto-button-selected';
                            that.imageAssign();
                            that.colorAssign();
                            justHover = true;
                        }
                    });
                    button.addEventListener('mouseleave', function () {
                        if (justHover == true && this.className.indexOf('sto-button-selected') > -1) {
                            this.className = 'sto-button sto-button-normal';
                            that.imageAssign();
                            that.colorAssign();
                            justHover = false;
                        }
                    });
                    button.addEventListener('click', function () {
                        let btnid = this.getAttribute('data-id');
                        for (k = 0; k < document.querySelectorAll('.sto-product-container>*').length; k++) {
                            document.querySelectorAll('.sto-product-container>*')[k].style.display = 'none';
                        }
                        document.querySelector('.sto-product-container>*[data-id="' + this.getAttribute('data-id') + '"]').style.display = 'block';
                        for (l = 0; l < document.querySelectorAll('button.sto-button').length; l++) {
                            document.querySelectorAll('button.sto-button')[l].className = 'sto-button sto-button-normal';
                        }
                        for (m = 0; m < document.querySelectorAll('button[data-id="' + btnid + '"]').length; m++) {
                            document.querySelectorAll('button[data-id="' + btnid + '"]')[m].className = 'sto-button sto-button-selected';
                        }
                        justHover = false;
                        that.trackFormat(
                            'trackBrowse', {
                            id: this.getAttribute('data-id'),
                            title: this.getAttribute('data-title')
                        }
                        );
                        that.imageAssign();
                        that.colorAssign();
                    });
                    buttonWrapper.appendChild(button);
                }
            }
        }

        buttonWrapper.querySelector('button.sto-button').className = 'sto-button sto-button-selected';

        if (that.settings.frame === 'buttons') {
            buttonWrapper = container.querySelector('.sto-buttons');
            if (that.settings['img_sto-button-normal'] && that.settings['img_sto-button-normal'] != '') {
                buttonWrapper.setAttribute('button-normal', 'img');
            } else {
                buttonWrapper.setAttribute('button-normal', 'color');
            }
            if (that.settings['img_sto-button-selected'] && that.settings['img_sto-button-selected'] != '') {
                buttonWrapper.setAttribute('button-selected', 'img');
            } else {
                buttonWrapper.setAttribute('button-selected', 'color');
            }
            for (let indexProduct in productsInSettings) {
                let pdts = productsInSettings[indexProduct][3];
                for (let indexProdsIds in pdts) {
                    if (container.querySelector('.sto-product-container>*[data-id="' + productsInSettings[indexProduct][3][parseFloat(indexProdsIds)] + '"]')) {

                        let button = document.createElement('button');
                        button.className = 'sto-button sto-button-normal';
                        button.setAttribute('data-index', indexProduct);
                        button.setAttribute('data-id', productsInSettings[indexProduct][3][parseFloat(indexProdsIds)]);
                        button.setAttribute('data-title', productsInSettings[indexProduct][1]);
                        button.style.width = this.settings.button_width && this.settings.button_width != '' ? this.settings.button_width + 'px' : '150px';
                        button.style.fontSize = this.settings.button_fontsize && this.settings.button_fontsize != '' ? this.settings.button_fontsize + 'px' : '13px';
                        button.innerText = productsInSettings[indexProduct][1];

                        let justHover2 = false;
                        button.addEventListener('mouseenter', function () {
                            if (this.className.indexOf('sto-button-selected') == -1) {
                                this.className = 'sto-button sto-button-selected';
                                that.imageAssign();
                                that.colorAssign();
                                justHover2 = true;
                            }
                        });
                        button.addEventListener('mouseleave', function () {
                            if (justHover2 == true && this.className.indexOf('sto-button-selected') > -1) {
                                this.className = 'sto-button sto-button-normal';
                                that.imageAssign();
                                that.colorAssign();
                                justHover2 = false;
                            }
                        });
                        button.addEventListener('click', function () {
                            let btnid = this.getAttribute('data-id');
                            for (k = 0; k < document.querySelectorAll('.sto-product-container>*').length; k++) {
                                document.querySelectorAll('.sto-product-container>*')[k].style.display = 'none';
                            }
                            document.querySelector('.sto-product-container>*[data-id="' + this.getAttribute('data-id') + '"]').style.display = 'block';
                            for (l = 0; l < document.querySelectorAll('button.sto-button').length; l++) {
                                document.querySelectorAll('button.sto-button')[l].className = 'sto-button sto-button-normal';
                            }
                            for (m = 0; m < document.querySelectorAll('button[data-id="' + btnid + '"]').length; m++) {
                                document.querySelectorAll('button[data-id="' + btnid + '"]')[m].className = 'sto-button sto-button-selected';
                            }
                            justHover2 = false;
                            that.trackFormat(
                                'trackBrowse', {
                                id: this.getAttribute('data-id'),
                                title: this.getAttribute('data-title')
                            }
                            );
                            that.imageAssign();
                            that.colorAssign();
                        });
                        buttonWrapper.appendChild(button);
                        break;
                    }
                }
            }

        }

        //arrowsHoverState
        if (container.querySelector('.sto-arrows')) {
            const arrows = container.querySelectorAll('.sto-arrows');
            for (let arrowInd = 0; arrowInd < arrows.length; arrowInd++) {
                arrows[arrowInd].addEventListener('mouseenter', function () {
                    let arrowClass = this.className.replace('sto-arrows', 'sto-arrows-hover');
                    this.className = arrowClass;
                    that.imageAssign();
                    that.colorAssign();
                });
                arrows[arrowInd].addEventListener('mouseleave', function () {
                    let arrowClass = this.className.replace('sto-arrows-hover', 'sto-arrows');
                    this.className = arrowClass;
                    that.imageAssign();
                    that.colorAssign();
                });
            }
        }

        buttonWrapper.querySelector('button.sto-button').className = 'sto-button sto-button-selected';
        this.imageAssign();
        this.colorAssign();
        return container;
    }
    wording(container) {
        const that = this;
        const format = container.querySelector('.sto-format');
        const wording = document.createElement('div');
        wording.className += 'sto-wording';
        wording.innerHTML += '<span>' + this.settings.wording + '</span>';
        if (that.settings.wording != '') {
            format.prepend(wording);
        }
    }
    checkNbProduct(prods) {
        let counterProducts = 0;

        prods.forEach(function (e) {
            if (crawl[e]) counterProducts++;
        });
    }
    htmlFactory() {
        let parser = new DOMParser();
        let htmlTreatment = __TYPE__ == 'api' ? document : parser.parseFromString(htmlImport, 'text/html');
        const htmlExport = htmlTreatment.querySelector('.sto-' + __TYPE__ + '-container');
        htmlExport.id = 'sto-format-' + this.type + '-' + this.settings.cid;
        htmlExport.className += ' sto-' + __TYPE__ + '-container-' + this.type + '-' + this.settings.cid;
        htmlExport.setAttribute('data-type', this.type);
        htmlExport.setAttribute('data-creaid', this.settings.cid);
        this.html = htmlExport;
    }
    init() {
        this.htmlFactory();

        let control = false;
        let control2 = false;
        let that = this;

        let payload = {
            html: this.html,
            tracker: this.tracker,
            settings: this.settings
        }
        this.mobile = new mobile(payload);

        const impInt = setInterval(function () {
            if (document.querySelectorAll('.sto-' + __TYPE__ + '-container-' + that.type + '-' + that.settings.cid).length > 0 && control === false && that.type !== 'BB') {
                if (that.type !== 'PMP') {
                    that.trackFormat('imp', 'null');
                    control = true;
                    clearInterval(impInt);
                } else {
                    if (that.settings.retailer != 'carrefour_one') {
                        const title = document.querySelector('.sto-product-container>*').getAttribute('data-title') !== null ? document.querySelector('.sto-product-container>*').getAttribute('data-title') : 'COMMING SOON UPDATE WITH TAG MODIFICATION';
                        const pid = document.querySelector('.sto-' + __TYPE__ + '-container-' + that.type + '-' + that.settings.cid).querySelector('.sto-product-container>*').getAttribute('data-id');
                        that.trackFormat('imp', {
                            tl: pid,
                            pi: pid,
                            pl: title
                        });
                        control = true;
                        clearInterval(impInt);
                    } else { }
                }
            }
        }, 500);
    }
}

export default format;
